# Sigma ABC | Fazenda/Talhao MS
Sigma - MicroServiço Fazenda/Talhao


## API

#dev
http://localhost:8080/swagger-ui.html <br />
#prod
http://sigma-abc.dsb.eldorado.org.br:8080/swagger-ui.html

## Build

gradlew build

## Running unit tests

gradlew test

### Push the container to the GCloud registry

docker push gcr.io/sigma-abc/sigma-abc-ms-fazenda:{TAG}

### Deploy the OpenAPI file

gcloud endpoints services deploy sigma-abc-fazenda-openapi.yaml

### Deploy to the kubernetes

kubectl apply -f sigma-abc-ms-fazenda-deployment.yaml

