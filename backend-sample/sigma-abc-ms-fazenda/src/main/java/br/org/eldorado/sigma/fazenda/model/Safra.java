package br.org.eldorado.sigma.fazenda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "safra", schema = "cadastro")
public class Safra {

	@Id
	@Column(name = "cod_safra")
	private int codigo;

	@Column(name = "nome")
	private String nome;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
