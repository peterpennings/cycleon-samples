package br.org.eldorado.sigma.fazenda.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.org.eldorado.sigma.fazenda.model.Talhao;
import br.org.eldorado.sigma.fazenda.repository.ITalhaoRepository;
import br.org.eldorado.sigma.fazenda.service.ITalhaoService;

@Service
public class TalhaoService implements ITalhaoService {

	@Autowired
	private ITalhaoRepository talhaoRepository;

	@Override
	public Talhao findById(int id) {
		return talhaoRepository.findById(id).orElse(null);
	}

	@Override
	public Collection<Talhao> findAll() {
		return (Collection<Talhao>) talhaoRepository.findAll();
	}

	@Override
	public Collection<Talhao> findByFazenda(Integer codigoFazenda) {
		return talhaoRepository.findByFazenda(codigoFazenda);
	}

	@Override
	public Talhao findByFazendaAndTalhao(Integer codigoFazenda, Integer codigoTalhao) {
		return talhaoRepository.findByFazendaAndTalhao(codigoFazenda, codigoTalhao);
	}

}
