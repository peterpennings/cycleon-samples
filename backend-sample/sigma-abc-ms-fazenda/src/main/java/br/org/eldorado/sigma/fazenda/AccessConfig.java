package br.org.eldorado.sigma.fazenda;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import br.org.eldorado.sigma.fazenda.interceptor.CheckAccess;

/**
 * 
 * @author peter.pennings
 *
 */
@Configuration
public class AccessConfig implements WebMvcConfigurer {

	private final List<String> urlsToIntercept = new ArrayList<String>() {

		private static final long serialVersionUID = 1L;

		{
			add("/fazendas/");
			add("/fazendas");
			add("/fazendas/produtor/{idProdutor}");
			add("/fazendas/produtor/{idProdutor}/");
			add("/fazendas/{id}/");
			add("/fazendas/{id}");
			add("/fazendas/{id}/talhoes/");
			add("/fazendas/{id}/talhoes");
			add("/fazendas/{id}/ocorrencias/");
			add("/fazendas/{id}/ocorrencias");
			add("/fazendas/{id}/ocorrencias/{idOcorrencia}/");
			add("/fazendas/{id}/ocorrencias/{idOcorrencia}");
		}
	};

	@Bean
	CheckAccess checkAccess() {
		return new CheckAccess();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(checkAccess()).addPathPatterns(urlsToIntercept);
		WebMvcConfigurer.super.addInterceptors(registry);
	}

}