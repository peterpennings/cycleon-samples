package br.org.eldorado.sigma.fazenda.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.org.eldorado.sigma.fazenda.model.Fazenda;
import br.org.eldorado.sigma.fazenda.repository.IFazendaRepository;
import br.org.eldorado.sigma.fazenda.service.IFazendaService;

@Service
public class FazendaService implements IFazendaService {

	@Autowired
	private IFazendaRepository fazendaRepository;

	@Override
	public Fazenda findById(int id) {
		return fazendaRepository.findById(id).orElse(null);
	}

	@Override
	public Collection<Fazenda> findAll() {
		return (Collection<Fazenda>) fazendaRepository.findAll();
	}
	
	@Override
	public Collection<Fazenda> findByCodigoPessoa(Integer codigoPessoa) {
		return fazendaRepository.findByCodigoPessoa(codigoPessoa);
	}

}
