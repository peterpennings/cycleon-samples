package br.org.eldorado.sigma.fazenda.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.org.eldorado.sigma.fazenda.model.Talhao;

@Repository
public interface ITalhaoRepository extends CrudRepository<Talhao, Integer> {
	
	@Query(value = "SELECT * FROM cadastro.talhao_safra u "
			+ "WHERE cod_talhao_safra IN ( "         
			+ "SELECT max(cod_talhao_safra) "
			+ "FROM cadastro.talhao_safra "
			+ "WHERE cod_fazenda=:codigoFazenda "
			+ "GROUP BY nome, cod_safra) ORDER BY u.nome", nativeQuery = true)
/*	@Query(value = "SELECT * FROM cadastro.talhao_safra u "
					  + "WHERE cod_talhao_safra IN ( "             
					  + "SELECT max(cod_talhao_safra) "
					  + "FROM cadastro.talhao_safra "
					  + "WHERE cod_fazenda=:codigoFazenda "
					  + "GROUP BY ST_AsBinary(the_geom)) ORDER BY u.nome", 
		   nativeQuery = true)*/
	Collection<Talhao> findByFazenda(@Param("codigoFazenda") Integer codigoFazenda);
	
	@Query("SELECT t FROM Talhao t INNER JOIN t.fazenda f WHERE f.codigo = :codigoFazenda AND t.codigo = :codigoTalhao")
	Talhao findByFazendaAndTalhao(@Param("codigoFazenda") Integer codigoFazenda, @Param("codigoTalhao") Integer codigoTalhao);
	
}