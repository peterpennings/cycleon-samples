package br.org.eldorado.sigma.fazenda.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import br.org.eldorado.sigma.fazenda.model.MvTalhaoSafraInfo;

@Service
public interface IMvTalhaoSafraInfoService {

	Collection<MvTalhaoSafraInfo> findAll();
	
	Collection<MvTalhaoSafraInfo> findByFazendaAndTalhao(Integer codigoFazenda, Integer codigoTalhao);
	
}
