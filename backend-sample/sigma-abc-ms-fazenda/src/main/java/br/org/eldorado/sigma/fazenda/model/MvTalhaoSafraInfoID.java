package br.org.eldorado.sigma.fazenda.model;

import java.io.Serializable;

public class MvTalhaoSafraInfoID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1974439304826367017L;

	private Integer codigoTalhaoSafra;

	private String idSementeCooperativa;

	public Integer getCodigoTalhaoSafra() {
		return codigoTalhaoSafra;
	}

	public void setCodigoTalhaoSafra(Integer codigoTalhaoSafra) {
		this.codigoTalhaoSafra = codigoTalhaoSafra;
	}

	public String getIdSementeCooperativa() {
		return idSementeCooperativa;
	}

	public void setIdSementeCooperativa(String idSementeCooperativa) {
		this.idSementeCooperativa = idSementeCooperativa;
	}

}
