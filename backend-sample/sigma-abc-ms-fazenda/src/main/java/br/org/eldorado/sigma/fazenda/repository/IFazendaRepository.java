package br.org.eldorado.sigma.fazenda.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.org.eldorado.sigma.fazenda.model.Fazenda;

@Repository
public interface IFazendaRepository extends CrudRepository<Fazenda, Integer> {
	
	@Query("SELECT t FROM Fazenda t INNER JOIN t.pessoas p WHERE p.codigoPessoa = :codigoPessoa ORDER BY t.nome")
	Collection<Fazenda> findByCodigoPessoa(@Param("codigoPessoa") Integer codigoPessoa);
	
}