package br.org.eldorado.sigma.fazenda.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vividsolutions.jts.geom.Geometry;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "fazenda", schema = "cadastro")
public class Fazenda {

	@Id
	@Column(name = "cod_fazenda")
	private int codigo;

	@Column(name = "nome")
	private String nome;

	@Column(name = "ponto_sede")
	private Geometry pontoSede;

	@Column(name = "the_geom")
	private Geometry poligono;
	
	@Formula("(SELECT p.nome FROM cadastro.pessoa_fazenda pf INNER JOIN cadastro.pessoa p on p.cod_pessoa = pf.cod_pessoa WHERE pf.cod_fazenda = cod_fazenda AND pf.cod_papel = 1)")
	private String nomeProdutor;

	@Column(name = "area_ha")
	private Double areaHectare;

	@OneToMany(mappedBy = "fazenda", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonBackReference
	private List<FazendaPessoa> pessoas = new ArrayList<>();

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Geometry getPontoSede() {
		return pontoSede;
	}

	public void setPontoSede(Geometry pontoSede) {
		this.pontoSede = pontoSede;
	}

	public Geometry getPoligono() {
		return poligono;
	}

	public void setPoligono(Geometry poligono) {
		this.poligono = poligono;
	}

	public Double getAreaHectare() {
		return areaHectare;
	}

	public void setAreaHectare(Double areaHectare) {
		this.areaHectare = areaHectare;
	}

	public List<FazendaPessoa> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<FazendaPessoa> pessoas) {
		this.pessoas = pessoas;
	}

	public String getNomeProdutor() {
		return nomeProdutor;
	}

	public void setNomeProdutor(String nomeProdutor) {
		this.nomeProdutor = nomeProdutor;
	}
	
	

}