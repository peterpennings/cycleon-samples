package br.org.eldorado.sigma.fazenda.api.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author andre.bettin
 *
 */
@RestController
@RequestMapping(value = "/")
@CrossOrigin
public class HealthCheckController {

    private static final String RESPONSE = "health check OK";

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> healthCheck() {
        return new ResponseEntity<String>(RESPONSE, HttpStatus.OK);
    }
}

