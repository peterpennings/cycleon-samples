package br.org.eldorado.sigma.fazenda.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import br.org.eldorado.sigma.fazenda.model.Talhao;

@Service
public interface ITalhaoService {

	Talhao findById(int id);

	Collection<Talhao> findAll();

	Collection<Talhao> findByFazenda(Integer codigoFazenda);

	Talhao findByFazendaAndTalhao(Integer codigoFazenda, Integer codigoTalhao);

}
