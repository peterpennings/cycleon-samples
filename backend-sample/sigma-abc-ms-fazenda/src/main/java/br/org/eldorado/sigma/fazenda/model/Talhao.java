package br.org.eldorado.sigma.fazenda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.vividsolutions.jts.geom.Geometry;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "talhao_safra", schema = "cadastro")
public class Talhao {

	@Id
	@Column(name = "cod_talhao_safra")
	private int codigo;

	@ManyToOne
	@JoinColumn(name = "cod_safra")
	private Safra safra;

	@Column(name = "nome")
	private String nome;

	@ManyToOne
	@JoinColumn(name = "cod_fazenda")
	private Fazenda fazenda;

	@Column(name = "tipo_safra")
	private String tipoSafra;

	@Column(name = "the_geom")
	// @JsonSerialize(using = GeometrySerializer.class)
	// @JsonDeserialize(using = GeometryDeserializer.class)
	private Geometry poligono;

	@Column(name = "centroide_x")
	private Double centroideX;

	@Column(name = "centroide_y")
	private Double centroideY;

	@Column(name = "area_ha")
	private Double areaHectare;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Safra getSafra() {
		return safra;
	}

	public void setSafra(Safra safra) {
		this.safra = safra;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Geometry getPoligono() {
		return poligono;
	}

	public void setPoligono(Geometry poligono) {
		this.poligono = poligono;
	}

	public Fazenda getFazenda() {
		return fazenda;
	}

	public void setFazenda(Fazenda fazenda) {
		this.fazenda = fazenda;
	}

	public String getTipoSafra() {
		return tipoSafra;
	}

	public void setTipoSafra(String tipoSafra) {
		this.tipoSafra = tipoSafra;
	}

	public Double getCentroideX() {
		return centroideX;
	}

	public void setCentroideX(Double centroideX) {
		this.centroideX = centroideX;
	}

	public Double getCentroideY() {
		return centroideY;
	}

	public void setCentroideY(Double centroideY) {
		this.centroideY = centroideY;
	}

	public Double getAreaHectare() {
		return areaHectare;
	}

	public void setAreaHectare(Double areaHectare) {
		this.areaHectare = areaHectare;
	}

}
