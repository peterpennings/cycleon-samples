package br.org.eldorado.sigma.fazenda.interceptor;

import java.util.Base64;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.org.eldorado.sigma.fazenda.model.Fazenda;
import br.org.eldorado.sigma.fazenda.service.IFazendaService;

/**
 * 
 * @author peter.pennings
 *
 */
@Component
public class CheckAccess extends HandlerInterceptorAdapter {

	@Autowired
	private IFazendaService fazendaService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// Variaveis de URL (Path)
		@SuppressWarnings("unchecked")
		Map<String, String> pathVariables = (Map<String, String>) request
				.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

		// Token (JWT)
		String auth = request.getHeader("Authorization");
		String[] tokenSplitted = auth.replace("Bearer", "").split("\\.");

		// Token Payload
		String payload = new String(Base64.getDecoder().decode(tokenSplitted[1]));
		JSONObject jsonObject = new JSONObject(payload);

		// Fazendas no Token
		//JSONArray fazendasJson = (JSONArray) jsonObject.get("user-f");

		String user_key = jsonObject.getString("user_id");
		request.setAttribute("user_id", user_key);

		Integer key = Integer.valueOf(user_key);
		Collection<Fazenda> userFazendas = fazendaService.findByCodigoPessoa(key);
		Collection<Integer> codigosFazendas = userFazendas.stream().map(Fazenda::getCodigo).collect(Collectors.toList());

		// verifica se a fazenda da URL esta no token
		Integer idFazenda = pathVariables.get("id") == null ? null : Integer.parseInt(pathVariables.get("id"));
		boolean allow = false;
		if (idFazenda == null) {
			allow = true;
		} else {
			for (Integer faz_key : codigosFazendas) {
				if (faz_key.equals(idFazenda)) {
					allow = true;
					break;
				}
			}
			/*
			 * for (int i = 0; i < fazendasJson.length(); ++i) { if
			 * (fazendasJson.getInt(i) == idFazenda) { allow = true; break; } }
			 */
		}

		if (!allow) {
			response.sendError(HttpStatus.PRECONDITION_FAILED.value(), "Codigo da fazenda desconhecido!");
			return false;
		}

		return super.preHandle(request, response, handler);

	}

}
