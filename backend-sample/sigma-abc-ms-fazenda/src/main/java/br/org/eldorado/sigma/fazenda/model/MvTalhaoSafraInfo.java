package br.org.eldorado.sigma.fazenda.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "mv_talhao_safra_info", schema = "sigma")
@IdClass(MvTalhaoSafraInfoID.class)
public class MvTalhaoSafraInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2111367642022001023L;

	@Column(name = "cod_fazenda")
	private Integer codigoFazenda;

	@Id
	@Column(name = "cod_talhao_safra")
	private Integer codigoTalhaoSafra;

	@Id
	@Column(name = "id_semente_coop")
	private String idSementeCooperativa;

	@Column(name = "cod_tipo_manejo")
	private Integer codigoTipoManejo;

	@Column(name = "cod_cultura")
	private Integer codigoCultura;

	@Column(name = "cod_cultivar")
	private Integer codigoCultivar;

	@Column(name = "ultimo_cod_estadio_avaliado")
	private Integer ultimoCodEstadioAvaliado;

	@Column(name = "nome_fazenda")
	private String nomeFazenda;

	@Column(name = "nome_talhao")
	private String nomeTalhao;

	@Column(name = "nome_cultura")
	private String nomeCultura;

	@Column(name = "nome_cultivar")
	private String nomeCultivar;

	@Column(name = "nome_cultivar_ws")
	private String nomeCultivarWs;

	@Column(name = "dh_ultima_avaliacao")
	private Date ultimaAvaliacao;

	@Column(name = "dh_ultimo_manejo_realizado")
	private Date ultimoManejoRealizado;

	@Column(name = "ultimo_nome_estadio_avaliado")
	private String ultimoNomeEstadioAvaliado;

	public Integer getCodigoFazenda() {
		return codigoFazenda;
	}

	public void setCodigoFazenda(Integer codigoFazenda) {
		this.codigoFazenda = codigoFazenda;
	}

	public Integer getCodigoTalhaoSafra() {
		return codigoTalhaoSafra;
	}

	public void setCodigoTalhaoSafra(Integer codigoTalhaoSafra) {
		this.codigoTalhaoSafra = codigoTalhaoSafra;
	}


	public String getIdSementeCooperativa() {
		return idSementeCooperativa;
	}

	public void setIdSementeCooperativa(String idSementeCooperativa) {
		this.idSementeCooperativa = idSementeCooperativa;
	}

	public Integer getCodigoTipoManejo() {
		return codigoTipoManejo;
	}

	public void setCodigoTipoManejo(Integer codigoTipoManejo) {
		this.codigoTipoManejo = codigoTipoManejo;
	}

	public Integer getCodigoCultura() {
		return codigoCultura;
	}

	public void setCodigoCultura(Integer codigoCultura) {
		this.codigoCultura = codigoCultura;
	}

	public Integer getCodigoCultivar() {
		return codigoCultivar;
	}

	public void setCodigoCultivar(Integer codigoCultivar) {
		this.codigoCultivar = codigoCultivar;
	}

	public Integer getUltimoCodEstadioAvaliado() {
		return ultimoCodEstadioAvaliado;
	}

	public void setUltimoCodEstadioAvaliado(Integer ultimoCodEstadioAvaliado) {
		this.ultimoCodEstadioAvaliado = ultimoCodEstadioAvaliado;
	}

	public String getNomeFazenda() {
		return nomeFazenda;
	}

	public void setNomeFazenda(String nomeFazenda) {
		this.nomeFazenda = nomeFazenda;
	}

	public String getNomeTalhao() {
		return nomeTalhao;
	}

	public void setNomeTalhao(String nomeTalhao) {
		this.nomeTalhao = nomeTalhao;
	}

	public String getNomeCultura() {
		return nomeCultura;
	}

	public void setNomeCultura(String nomeCultura) {
		this.nomeCultura = nomeCultura;
	}

	public String getNomeCultivar() {
		return nomeCultivar;
	}

	public void setNomeCultivar(String nomeCultivar) {
		this.nomeCultivar = nomeCultivar;
	}

	public String getNomeCultivarWs() {
		return nomeCultivarWs;
	}

	public void setNomeCultivarWs(String nomeCultivarWs) {
		this.nomeCultivarWs = nomeCultivarWs;
	}

	public Date getUltimaAvaliacao() {
		return ultimaAvaliacao;
	}

	public void setUltimaAvaliacao(Date ultimaAvaliacao) {
		this.ultimaAvaliacao = ultimaAvaliacao;
	}

	public Date getUltimoManejoRealizado() {
		return ultimoManejoRealizado;
	}

	public void setUltimoManejoRealizado(Date ultimoManejoRealizado) {
		this.ultimoManejoRealizado = ultimoManejoRealizado;
	}

	public String getUltimoNomeEstadioAvaliado() {
		return ultimoNomeEstadioAvaliado;
	}

	public void setUltimoNomeEstadioAvaliado(String ultimoNomeEstadioAvaliado) {
		this.ultimoNomeEstadioAvaliado = ultimoNomeEstadioAvaliado;
	}

}
