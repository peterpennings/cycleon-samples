package br.org.eldorado.sigma.fazenda.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.org.eldorado.sigma.fazenda.model.MvTalhaoSafraInfo;
import br.org.eldorado.sigma.fazenda.model.MvTalhaoSafraInfoID;

@Repository
public interface IMvTalhaoSafraInfoRepository extends CrudRepository<MvTalhaoSafraInfo, MvTalhaoSafraInfoID> {

	@Query("SELECT t FROM MvTalhaoSafraInfo t "
			+ "WHERE t.codigoFazenda = :fazenda "
			+ "AND ((t.codigoTalhaoSafra = :talhao) OR (:talhao IS NULL))")
	public Collection<MvTalhaoSafraInfo> findByFazendaAndTalhao(@Param("fazenda") Integer codigoFazenda, @Param("talhao") Integer codigoTalhao);
	
}
