package br.org.eldorado.sigma.fazenda.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import br.org.eldorado.sigma.fazenda.model.Fazenda;

@Service
public interface IFazendaService {
	
	Fazenda findById(int id);

	Collection<Fazenda> findAll();
	
	Collection<Fazenda> findByCodigoPessoa(Integer codigoPessoa);
	
}
