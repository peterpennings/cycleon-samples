package br.org.eldorado.sigma.fazenda.api.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.org.eldorado.sigma.fazenda.model.Fazenda;
import br.org.eldorado.sigma.fazenda.model.MvTalhaoSafraInfo;
import br.org.eldorado.sigma.fazenda.model.Talhao;
import br.org.eldorado.sigma.fazenda.service.IFazendaService;
import br.org.eldorado.sigma.fazenda.service.IMvTalhaoSafraInfoService;
import br.org.eldorado.sigma.fazenda.service.ITalhaoService;

/**
 * 
 * @author peter.pennings
 *
 */
@RestController
@RequestMapping(value = "/fazendas")
@CrossOrigin
public class FazendaResourceController {

	@Autowired
	private IFazendaService fazendaService;

	@Autowired
	private ITalhaoService talhaoService;

	@Autowired
	private IMvTalhaoSafraInfoService mvTalhaoSafraInfoService;

	/**
	 * Busca fazendas em comum entre produtor e agronomo logado
	 * 
	 * @param userId
	 * @param codigoProdutor
	 * @return
	 */
	@RequestMapping(value = "/produtor/{idProdutor}", method = RequestMethod.GET)
	public ResponseEntity<Collection<Fazenda>> getFazendasByProdutor(@RequestAttribute("user_id") String userId,
			@PathVariable int idProdutor) {
		Integer uid = null;
		Collection<Fazenda> fazendasUserLogado = null;
		try {
			uid = Integer.parseInt(userId);
			fazendasUserLogado = fazendaService.findByCodigoPessoa(uid);
		} catch (Exception e) {
			System.out.println("User ID invalido");
			return new ResponseEntity<Collection<Fazenda>>(new ArrayList<Fazenda>(), HttpStatus.OK);
		}

		Collection<Fazenda> fazendasProdutor = fazendaService.findByCodigoPessoa(idProdutor);
		ArrayList<Fazenda> resultList = new ArrayList<Fazenda>();
		for (Fazenda fazUser : fazendasUserLogado) {
			for (Fazenda fazProd : fazendasProdutor) {
				if (fazUser.getCodigo() == fazProd.getCodigo()) {
					resultList.add(fazProd);
					break;
				}
			}
		}

		resultList.sort(Comparator.comparing(Fazenda::getNome));

		return new ResponseEntity<Collection<Fazenda>>(resultList, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Fazenda> get(@PathVariable int id) {
		Fazenda fazenda = fazendaService.findById(id);
		if (fazenda == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Fazenda>(fazenda, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<Fazenda>> getAll(@RequestAttribute("user_id") String userId) {
		Integer uid = null;
		Collection<Fazenda> fazendas = null;
		try {
			uid = Integer.parseInt(userId);
			fazendas = fazendaService.findByCodigoPessoa(uid);
		} catch (Exception e) {
			System.out.println("User ID invalido");
		}

		return new ResponseEntity<Collection<Fazenda>>(fazendas, HttpStatus.OK);

	}

	@RequestMapping(value = "/{id}/talhoes", method = RequestMethod.GET)
	public ResponseEntity<Collection<Talhao>> getTalhoes(@PathVariable int id) {
		Collection<Talhao> talhoes = talhaoService.findByFazenda(id);
		return new ResponseEntity<Collection<Talhao>>(talhoes, HttpStatus.OK);

	}

	@RequestMapping(value = "/{id}/talhoes/{idTalhao}", method = RequestMethod.GET)
	public ResponseEntity<Talhao> getByFazendaAndTalhao(@PathVariable Integer id, @PathVariable Integer idTalhao) {
		Talhao talhao = talhaoService.findByFazendaAndTalhao(id, idTalhao);
		if (talhao == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Talhao>(talhao, HttpStatus.OK);

	}

	@RequestMapping(value = "/{id}/resumo", method = RequestMethod.GET)
	public ResponseEntity<Collection<MvTalhaoSafraInfo>> getResumo(@PathVariable Integer id,
			@RequestParam(name = "talhao", required = false) Integer idTalhao) {
		Collection<MvTalhaoSafraInfo> resultList = new ArrayList<MvTalhaoSafraInfo>();
		resultList = mvTalhaoSafraInfoService.findByFazendaAndTalhao(id, idTalhao);

		return new ResponseEntity<Collection<MvTalhaoSafraInfo>>(resultList, HttpStatus.OK);

	}

}