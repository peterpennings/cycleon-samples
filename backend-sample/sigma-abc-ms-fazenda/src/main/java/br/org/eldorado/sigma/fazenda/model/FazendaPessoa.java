package br.org.eldorado.sigma.fazenda.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "pessoa_fazenda", schema = "cadastro")
public class FazendaPessoa implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "cod_pessoa_fazenda")
	private int codigoPessoaFazenda;

	@ManyToOne
	@JoinColumn(name = "cod_fazenda")
	@JsonBackReference
	private Fazenda fazenda;

	@Column(name = "cod_pessoa")
	private int codigoPessoa;

	@Column(name = "cod_papel")
	private int codigoPapel;

	public Fazenda getFazenda() {
		return fazenda;
	}

	public void setFazenda(Fazenda fazenda) {
		this.fazenda = fazenda;
	}

	public int getCodigoPessoa() {
		return codigoPessoa;
	}

	public void setCodigoPessoa(int codigoPessoa) {
		this.codigoPessoa = codigoPessoa;
	}

	public int getCodigoPapel() {
		return codigoPapel;
	}

	public void setCodigoPapel(int codigoPapel) {
		this.codigoPapel = codigoPapel;
	}

}
