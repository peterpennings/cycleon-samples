package br.org.eldorado.sigma.fazenda.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.org.eldorado.sigma.fazenda.model.MvTalhaoSafraInfo;
import br.org.eldorado.sigma.fazenda.repository.IMvTalhaoSafraInfoRepository;
import br.org.eldorado.sigma.fazenda.service.IMvTalhaoSafraInfoService;

@Service
public class MvTalhaoSafraInfoService implements IMvTalhaoSafraInfoService {

	@Autowired
	private IMvTalhaoSafraInfoRepository mvTalhaoSafraInfoRepository;
	
	@Override
	public Collection<MvTalhaoSafraInfo> findAll() {
		return (Collection<MvTalhaoSafraInfo>) mvTalhaoSafraInfoRepository.findAll();
	}

	@Override
	public Collection<MvTalhaoSafraInfo> findByFazendaAndTalhao(Integer codigoFazenda, Integer codigoTalhao) {
		return mvTalhaoSafraInfoRepository.findByFazendaAndTalhao(codigoFazenda, codigoTalhao);
	}
	
	
}
