package br.org.eldorado.sigma.fazenda;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class FazendaApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(FazendaApplication.class, args);
	}

}
