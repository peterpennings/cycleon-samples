#!/bin/bash

# if there is no parameter we will tag containers as latest
if [ -z "$1" ]; then
    TAG="latest"
else
    TAG=$1
fi

docker build --target openjdk-base -t sigma-abc-ms-fazenda/openjdk-base:$TAG .
docker build --target fazenda-build -t sigma-abc-ms-fazenda/fazenda-build:$TAG .
docker build --target release -t gcr.io/sigma-abc/sigma-abc-ms-fazenda:$TAG .

