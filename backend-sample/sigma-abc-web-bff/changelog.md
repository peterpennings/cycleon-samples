# Changelog

Todas as modificações importantes no projeto devem ser informadas nesse arquivo.

## [1.0.16] - 2019-06-12

### Added

- Melhorando tempo de carregamento do base em até 400% [(SIG-438)](https://contemp.atlassian.net/browse/SIG-438)

## [1.0.15] - 2019-06-09

### Added

- Método para consumir info dos talhões de uma fazenda [(SIG-429)](https://contemp.atlassian.net/browse/SIG-429)

## [1.0.14] - 2019-05-31

### Fixed

- Safe check para ponto sede de fazendas [(SIG-395)](https://contemp.atlassian.net/browse/SIG-395)
