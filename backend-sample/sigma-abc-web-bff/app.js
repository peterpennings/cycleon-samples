var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var corsConfig = require('./config/cors');

var healthRouter = require('./routes/health');
var baseRouter = require('./routes/base');
var ocorrenciasRouter = require('./routes/ocorrencias');
var satelitesRouter = require('./routes/satelites');
var climaRouter = require('./routes/clima');
var agroRouter = require('./routes/agro');

var app = express();
const expressSwagger = require('express-swagger-generator')(app);

// swagger generation
let options = {
  swaggerDefinition: {
    info: {
      description: 'BFF Sigma',
      title: 'SigmaABC Backend for Frontend',
      version: '1.1'
    },
    host: '127.0.0.1:3000',
    basePath: '/',
    produces: ['application/json'],
    schemes: ['http', 'https'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description: 'Token JWT para autenticação'
      }
    }
  },
  basedir: __dirname, //app absolute path
  files: ['./routes/**/*.js'] //Path to the API handle folder
};
expressSwagger(options);

// enable options to all
app.options('*', cors(corsConfig));

// handle cors and token for every call
app.use('/bff', cors(corsConfig), (req, res, next) => {
  // skip token para health check
  if (req.url === '/') {
    next();
  } else {
    // check token
    let access_token = req.headers['authorization'];

    if (!access_token) {
      // verifica token como parametro
      access_token = req.query.token;
      req.headers['authorization'] = `Bearer ${access_token}`;
    }

    // nao tem token
    if (access_token === undefined) {
      res.status(401).send('JWT validation failed.');
    } else {
      // res.header('Cache-Control', 'public, max-age=3600');
      next();
    }
  }
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', healthRouter);
app.use('/bff/base', baseRouter);
app.use('/bff/ocorrencias', ocorrenciasRouter);
app.use('/bff/satelites', satelitesRouter);
app.use('/bff/clima', climaRouter);
app.use('/bff/agro', agroRouter);

module.exports = app;
