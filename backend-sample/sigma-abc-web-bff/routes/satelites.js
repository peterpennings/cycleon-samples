var axios = require('../config/axios-instance-config');
var express = require('express');
var router = express.Router();

const BASE_FABC_URI = 'https://api.fundacaoabc.org/sigmaabc/indice-vegetacao';

/**
 * Busca datas para as quais existem imagens de satelite
 * @param {number} talhaoId ID do talhao
 */
function getDatasIV(talhaoId) {
  let URI = `${BASE_FABC_URI}/datas/${talhaoId}`;
  return axios.get(URI);
}

/**
 * Busca dados para montar grafico de histograma no popup de satelites
 * @param {number} talhaoId ID do talhao
 * @param {number} camada NDVI, EVI ou RGB
 * @param {string} data yyyy-MM-dd
 */
function getHistogramaIV(talhaoId, camada, dataInicio, dataFim) {
  // URI para uma data
  let URI = `${BASE_FABC_URI}/histogram/${talhaoId}/${dataInicio}/${camada}`;

  // chamada com range de datas
  if (dataFim) {
    URI = `${BASE_FABC_URI}/histogram/${talhaoId}/${dataInicio}/${dataFim}/${camada}`;
  }
  return axios.get(URI);
}

/**
 * Busca stats para um determinado tipo de layer satelite
 * @param {number} talhaoId ID do talhao
 * @param {number} tipo NDVI, EVI ou RGB
 * @param {string} dataInicio yyyy-MM-dd
 * @param {string} dataFim yyyy-MM-dd
 */
function getStats(talhaoId, tipo, dataInicio, dataFim) {
  let URI = `${BASE_FABC_URI}/stats/${talhaoId}/${dataInicio}/${dataFim}/${tipo}`;
  console.log(URI);
  return axios.get(URI);
}

/**
 * Retorna lista de datas para as quais existem imagens de satélite
 * para um determinado Talhão
 * @route GET /bff/satelites/iv/{codigoTalhao}/datas
 * @group satelites - Endpoint Satélites
 * @operationId satelitesIVDatasGET
 * @produces application/json
 * @param {number} codigoTalhao.path.required - Código numérico do talhão
 * @returns {object} 200 - Lista de datas disponíveis para talhão
 * @returns {Error}  500 - Erro ao tentar carregar datas
 * @security JWT
 */
router.get('/iv/:codigoTalhao/datas', function(req, res, next) {
  const talhao = req.params.codigoTalhao;

  // busca datas
  getDatasIV(talhao)
    .then(response => {
      const datas = response.data.data;
      res.status(200).send(datas);
    })
    .catch(error => {
      console.log(error);
      res.status(500).send('Erro ao tentar carregar datas para IV.');
    });
});

/**
 * Retorna lista de dados para histograma para um determinado Talhão
 * e Tipo. Os três tipos disponíveos são RGB, EVI e NDVI.
 * @route GET /bff/satelites/iv/{codigoTalhao}/histograma/{tipo}
 * @group satelites - Endpoint Satélites
 * @operationId satelitesIVHistogramaGET
 * @produces application/json
 * @param {number} codigoTalhao.path.required - Código numérico do talhão
 * @param {string} tipo.path.required - Tipo de dados (RGB, EVI ou NDVI)
 * @param {string} dataInicio.query.required - YYYY-MM-DD Data inicial
 * @param {string} dataFim.query - YYYY-MM-DD Data final
 * @returns {object} 200 - Lista de dados para histograma
 * @returns {Error}  500 - Erro ao tentar carregar dados para histograma
 * @security JWT
 */
router.get('/iv/:codigoTalhao/histograma/:tipo/', (req, res, next) => {
  const talhao = req.params.codigoTalhao;
  const tipo = req.params.tipo;

  // datas
  const dataInicio = req.query['dataInicio'];
  const dataFim = req.query['dataFim'];

  // busca dados do histograma
  getHistogramaIV(talhao, tipo, dataInicio, dataFim)
    .then(response => {
      res.status(200).send(response.data.data);
    })
    .catch(error => {
      console.log(error);
      res.status(500).send('Erro ao carregar histograma.');
    });
});

/**
 * Busca stats para imagens de satelite
 */

/**
 * Retorna stats para imagens de satélite para um determinado Talhão
 * e Tipo. Os três tipos disponíveos são RGB, EVI e NDVI.
 * @route GET /bff/satelites/iv/{codigoTalhao}/stats/{tipo}
 * @group satelites - Endpoint Satélites
 * @operationId satelitesIVStatsGET
 * @produces application/json
 * @param {number} codigoTalhao.path.required - Código numérico do talhão
 * @param {string} tipo.path.required - Tipo de dados (RGB, EVI ou NDVI)
 * @param {string} dataInicio.query.required - YYYY-MM-DD Data inicial
 * @param {string} dataFim.query - YYYY-MM-DD Data final
 * @returns {object} 200 - Lista de dados para histograma
 * @returns {Error}  500 - Erro ao tentar carregar dados para histograma
 * @security JWT
 */
router.get('/iv/:codigoTalhao/stats/:tipo/', (req, res, next) => {
  const talhao = req.params.codigoTalhao;
  const tipo = req.params.tipo;

  // datas
  const dataInicio = req.query['dataInicio'];
  const dataFim = req.query['dataFim'];

  // busca stats
  getStats(talhao, tipo, dataInicio, dataFim)
    .then(response => {
      res.status(200).send(response.data);
    })
    .catch(error => {
      console.log(error);
      res.status(500).send('Erro ao carregar stats');
    });
});

module.exports = router;
