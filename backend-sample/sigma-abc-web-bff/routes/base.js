var axios = require('../config/axios-instance-config');
var express = require('express');
var router = express.Router();
var baseController = require('../controllers/base-controller');
var fazendaController = require('../controllers/fazenda-controller');

/**
 * Retorna todas as fazendas associadas ao usuário autenticado.
 * A verificação do usuário ocorre no backend automaticamente
 * através do token.
 * @route GET /bff/base
 * @group base - Endpoint Base
 * @operationId fazendasGET
 * @produces application/json
 * @returns {object} 200 - Objeto contendo lista de Fazendas, Fazenda default
 * e Talhões para Fazenda default
 * @returns {Error}  500 - Erro ao tentar carregar fazendas
 * @security JWT
 */
router.get('/', async (req, res, next) => {
  const token = req.headers['authorization'];
  axios.defaults.headers.common['Authorization'] = token;

  try {
    // busca todas as fazendas
    const fazendasResponse = await fazendaController.getFazendas();
    if (fazendasResponse.status === 200) {
      const fazendas = fazendasResponse.data;

      // verifica se existem fazendas para o usuario
      if (fazendas.length === 0) {
        // retorna objeto vazio com 200 para nao precisar alterar
        // os tratamentos no portal
        res.status(200).send({
          fazenda: null,
          fazendas: [],
          talhoes: []
        });
      } else {
        const talhoesQueue = [];
        const infoQueue = [];

        // busca talhoes para todas as fazendas
        for (let i = 0; i < fazendas.length; i++) {
          const fazenda = fazendas[i];
          if (fazenda.pontoSede) {
            fazenda.ponto_sede = {
              lat: fazenda.pontoSede.coordinates[1],
              lng: fazenda.pontoSede.coordinates[0]
            };
          }

          // lista para paralelizar chamadas para os talhoes e info
          infoQueue.push(fazendaController.getResumo(fazenda.codigo));
          talhoesQueue.push(
            fazendaController.getTalhaoByFazenda(fazenda.codigo)
          );
        }

        // busca todos os talhoes
        const talhoesPorFazendaArray = [];
        Promise.all(talhoesQueue)
          .then(responses => {
            responses.forEach(resTalhoes => {
              talhoesPorFazendaArray.push(resTalhoes.data);
            });
          })
          .then(() => {
            // busca info dos talhoes
            Promise.all(infoQueue).then(resInfos => {
              resInfos.forEach((resInfo, index) => {
                talhoesPorFazendaArray[index].forEach(ta => {
                  ta.info = resInfo.data.filter(
                    r => r.codigoTalhaoSafra === ta.codigo
                  )[0];
                });
              });

              // carrega talhoes nas fazendas
              fazendas.forEach((fazenda, index) => {
                fazenda.talhoes = talhoesPorFazendaArray[index];
              });

              // adiciona lista de talhoes para nao quebrar UI
              // mas pode ser removido depois e os talhoes usados
              // sempre das fazendas
              // envia response
              res.status(200).json({
                fazenda: fazendas[0],
                fazendas: fazendas,
                talhoes: fazendas[0].talhoes
              });
            });
          });
      }
    }
  } catch (err) {
    console.log(err);
    res.status(500).send('Erro ao tentar inicializar sistema.');
  }
});

/**
 * Retorna lista de talhoes para determinada fazenda
 * @route GET /bff/base/fazendas/{codigoFazenda}/talhoes
 * @group base - Endpoint Base
 * @operationId fazendasTalhoesGET
 * @produces application/json
 * @param {number} codigoFazenda.path.required - Código da fazenda
 * @returns {object} 200 - Lista de Talhões
 * @returns {Error}  500 - Erro ao tentar carregar lista de Talhões
 * @security JWT
 */
router.get('/fazendas/:codigoFazenda/talhoes', (req, res, next) => {
  const token = req.headers['authorization'];
  axios.defaults.headers.common['Authorization'] = token;

  // busca talhoes para a fazenda
  fazendaController
    .getTalhaoByFazenda(req.params.codigoFazenda)
    .then(talhoesResponse => {
      res.status(200).send(talhoesResponse.data);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send(err);
    });
});

/**
 * Retorna uma pessoa
 * @route GET /bff/base/pessoas/{id}
 * @group base - Endpoint Base
 * @operationId pessoasGET
 * @produces application/json
 * @param {number} id.path.required - ID da pessoa
 * @returns {object} 200 - Pessoa
 * @returns {Error}  500 - Erro ao tentar carregar Pessoa
 * @security JWT
 */
router.get('/pessoas/:id', (req, res, next) => {
  const token = req.headers['authorization'];
  axios.defaults.headers.common['Authorization'] = token;

  baseController
    .getPessoaByCodigo(req.params.id)
    .then(pessoa => {
      res.status(200).send(pessoa);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send('Erro ao tentar buscar pessoa');
    });
});

module.exports = router;
