var express = require('express');
var router = express.Router();

var https = require('https');

/**
 * Healt check para BFF.
 * Este metodo e chamado pela web antes do login para
 * assegurar que o BFF esta disponivel.
 */
/**
 * Health check
 * @route GET /
 * @group / - Health Check
 * @operationId healthGET
 * @produces application/json
 * @returns {string} 200 - BFF OK
 * @security JWT
 */
router.get('/', function(req, res, next) {
  res.status(200).json('BFF OK');
});

module.exports = router;
