var axios = require('../config/axios-instance-config');
var express = require('express');
var router = express.Router();

// controller com funcoes basicas para CRUD
var controller = require('../controllers/ocorrencia-controller');

/**
 * Busca todas as ocorrencias de uma fazenda
 */
function getOcorrencias(fazendaId) {
  return axios.get(`fazendas/${fazendaId}/ocorrencias`);
}

/**
 * Busca todos os talhoes de uma fazenda
 */
function getTalhoes(fazendaId) {
  return axios.get(`fazendas/${fazendaId}/talhoes`);
}

/**
 * Retorna lista de ocorrencias para a fazenda informada
 * @route GET /bff/ocorrencias/{codigoFazenda}
 * @group ocorrencias - Endpoint Ocorrências
 * @operationId ocorrenciasGET
 * @produces application/json
 * @param {number} codigoFazenda.path.required - Código numérico da fazenda
 * @returns {object} 200 - Lista de Ocorrências
 * @returns {Error}  500 - Erro ao tentar carregar ocorrências
 * @security JWT
 */
router.get('/:codigoFazenda', function(req, res, next) {
  // define token axios
  const options = {
    headers: { authorization: req.headers['authorization'] }
  };
  axios(options);

  const fazendaId = req.params.codigoFazenda;

  // busca ocorrencias e talhoes
  Promise.all([getOcorrencias(fazendaId), getTalhoes(fazendaId)])
    .then(results => {
      let ocorrencias = {};
      let talhoes = {};
      let response = [];

      // extrai conteudo das ocorrencias e talhoes
      ocorrencias = results[0].data;
      talhoes = results[1].data;

      // carrega dados para listagem
      ocorrencias.forEach(ocorrencia => {
        const localPinCoords = ocorrencia.localPin.coordinates;

        // ajusta nome talhao
        const talhao = talhoes.find(t => t.codigo == ocorrencia.codTalhaoSafra);
        let local = 'Não definido';
        if (talhao !== undefined) {
          local = talhao.nome;
        }

        response.push({
          codigo: ocorrencia.codOcorrencia,
          estado: ocorrencia.estado,
          local: local,
          data: ocorrencia.unixTimeStampCadastro,
          origem: ocorrencia.tipoAcesso === 'APP' ? 'Aplicativo' : 'Web',
          responsavel: ocorrencia.codPessoa,
          resumo: ocorrencia.descricao,
          location: { lat: localPinCoords[1], lng: localPinCoords[0] }
        });
      });
      res.json(response);
    })
    .catch(error => {
      console.log(error);
      res
        .status(error.response.status)
        .send('Erro ao tentar carregar ocorrências');
    });
});

/**
 * Retorna uma Ocorrência
 * @route GET /bff/ocorrencias/{codigoFazenda}/{codigoOcorrencia}
 * @group ocorrencias - Endpoint Ocorrências
 * @operationId ocorrenciaGET
 * @produces application/json
 * @param {number} codigoFazenda.path.required - Código numérico da fazenda
 * @param {number} codigoOcorrencia.path.required - Código numérico da ocorrência
 * @returns {object} 200 - Ocorrência
 * @returns {object} 404 - Ocorrência não encontrada
 * @returns {Error}  500 - Erro ao tentar carregar ocorrência
 * @security JWT
 */
router.get('/:codigoFazenda/:codigoOcorrencia', function(req, res, next) {
  const fazendaId = req.params.codigoFazenda;
  const ocorrenciaId = req.params.codigoOcorrencia;

  controller
    .getOcorrencia(fazendaId, ocorrenciaId)
    .then(ocorrenciaResponse => {
      // TODO: Implementar caso de 404.
      res.json(ocorrenciaResponse.data);
    })
    .catch(error => {
      console.log(error);
      res.status(500).send('some error');
    });
});

/**
 * Retorna uma Ocorrência
 * @route POST /bff/ocorrencias/criar
 * @group ocorrencias - Endpoint Ocorrências
 * @operationId ocorrenciaPOST
 * @produces application/json
 * @returns {object} 200 - Ocorrência
 * @returns {object} 404 - Ocorrência não encontrada
 * @returns {Error}  500 - Erro ao tentar carregar ocorrência
 * @security JWT
 */
router.post('/criar', (req, res, next) => {
  const unix_ts = Math.floor(Date.now() / 1000);

  // position LatLng
  const position = req.body.position;

  // define ponto de criacao da ocorrencia
  const the_geom = {
    type: 'Point',
    coordinates: [position.lng, position.lat]
  };

  const payload = {
    estado: 'Aberta',
    tipoAcesso: 'WEB',
    descricao: req.body.titulo,
    codFazenda: req.body.fazendaId,
    codPessoa: req.body.userId,
    unixTimeStampCadastro: unix_ts,
    localPin: the_geom
  };

  // post para servico
  controller
    .criarAtividade(payload)
    .then(status => {
      res.status(200).send(status);
    })
    .catch(err => {
      console.log(err);
    });
});

module.exports = router;
