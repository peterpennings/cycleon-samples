var axios = require('../config/axios-instance-config');
var express = require('express');
var router = express.Router();

const MET_API_PREVISAO =
  'https://api.fundacaoabc.org/meteoro/1.0.0/previsao-tempo';
const MODELO_PREVISAO = 'gfs5_diario';

/**
 * Busca no servico lista de variaveis
 */
function getAllVariaveis() {
  return axios.get('/clima/variaveis');
}

/**
 * Busca no servico lista de estacoes metereologicas
 */
function getEstacoes() {
  return axios.get('/clima/estacoes');
}

/**
 * Busca estacao associada ao talhao, retorna null quando nao houver
 * @param {number} codigoTalhao Codigo do talhao
 */
function getEstacaoByTalhaoCodigo(codigoTalhao) {
  return axios.get(`/clima/talhoes/${codigoTalhao}/estacoes`);
}

/**
 * Busca as previsoes de todas as variaveis climaticas
 * @param {number} lat latitude
 * @param {number} lng longitude
 */
function getAllvariableValues(lat, lng) {
  return axios.get(`${MET_API_PREVISAO}/${lat}/${lng}/${MODELO_PREVISAO}`);
}

/**
 * Busca o valor de uma determinada variavel no dia especificado
 * @param {Array} data lista de variaveis
 * @param {number} variable variavel a ser retornada
 * @param {Date} date data de previsao para o valor da variavel
 */
function getVariableValue(data, variable, date) {
  return data.filter(function(item) {
    var dt = new Date(item.data_hora);
    return (
      item.cod_var == variable &&
      dt.getDate() == date.getDate() &&
      dt.getMonth() == date.getMonth() &&
      dt.getFullYear() == date.getFullYear()
    );
  });
}

/**
 * Retorna lista de todas as variáveis metereológicas do sistema
 * @route GET /bff/clima/variaveis
 * @group clima - Endpoint Clima
 * @operationId variaveisGET
 * @produces application/json
 * @returns {object} 200 - Lista de Variaveis
 * @returns {Error}  500 - Erro ao tentar carregar variáveis
 * @security JWT
 */
router.get('/variaveis', (req, res, next) => {
  const token = req.headers['authorization'];
  axios.defaults.headers.common['Authorization'] = token;

  getAllVariaveis()
    .then(response => {
      res.status(200).send(response.data);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send('Erro ao tentar carregar variáveis.');
    });
});

/**
 * Retorna lista de todas as estações metereológicas do sistema
 * @route GET /bff/clima/estacoes
 * @group clima - Endpoint Clima
 * @operationId estacoesGET
 * @produces application/json
 * @returns {object} 200 - Lista de Estações
 * @returns {Error}  500 - Erro ao tentar carregar estações
 * @security JWT
 */
router.get('/estacoes', (req, res, next) => {
  const token = req.headers['authorization'];
  axios.defaults.headers.common['Authorization'] = token;

  getEstacoes()
    .then(response => {
      res.status(200).send(response.data);
    })
    .catch(err => {
      res.status(500).send('Erro ao tentar carregar estações.');
      console.log(err);
    });
});

/**
 * Retorna uma estação metereológica associada a um talhão
 * @route GET /bff/clima/estacaoTalhao/{codigoTalhao}
 * @group clima - Endpoint Clima
 * @operationId estacaoTalhaoGET
 * @produces application/json
 * @param {number} codigoTalhao.path.required - Código numérico do talhão
 * @returns {object} 200 - Estação
 * @returns {Error}  404 - Nenhuma associação encontrada
 * @returns {Error}  500 - Erro ao tentar carregar estação
 * @security JWT
 */
router.get('/estacaoTalhao/:codigoTalhao', (req, res, next) => {
  const token = req.headers['authorization'];
  axios.defaults.headers.common['Authorization'] = token;
  const codigoTalhao = req.params.codigoTalhao;

  getEstacaoByTalhaoCodigo(codigoTalhao)
    .then(response => {
      res.status(200).send(response.data);
    })
    .catch(err => {
      let responseMessage = `Erro ao tentar carregar estação para talhão ${codigoTalhao}.`;
      const responseStatus = err.response.status;

      // verifica retornos de 404
      if (responseStatus === 404) {
        res.status(200).send(undefined);
      } else {
        // response
        res.status(err.response.status).send(responseMessage);
      }
    });
});

/**
 * Retorna Temperatura Máxima e Mínima do dia corrente para determinada lat lon
 * @route GET /bff/clima/temperatura/maxmin
 * @operationId temperaturaMaxMinGET
 * @produces application/json
 * @group clima - Endpoint Clima
 * @param {integer} lat.query.required - Latitude
 * @param {integer} lon.query.required - Longitude
 * @returns {object} 200 - Objeto contendo tempMin, tempMax e unidadeMedida
 * @returns {Error}  500 - Serviço indisponível
 * @security JWT
 */
router.get('/temperatura/maxmin', function(req, res, next) {
  const options = {
    headers: { authorization: req.headers['authorization'] }
  };
  axios(options);

  const latitude = req.query['lat'];
  const longitude = req.query['lon'];

  getAllvariableValues(latitude, longitude)
    .then(response => {
      var currentDate = new Date();
      currentDate.setHours(0, 0, 0, 0);

      try {
        var result = {
          tempMin: getVariableValue(response.data.data, 11, currentDate)[0]
            .valor,
          tempMax: getVariableValue(response.data.data, 12, currentDate)[0]
            .valor,
          unidadeMedida: '°C'
        };

        res.json(result);
      } catch (error) {
        console.log(error);
        res.status(500).send({ msg: 'Serviço indisponível' });
      }
    })
    .catch(err => {
      console.log(err);
    });
});

module.exports = router;
