var axios = require('../config/axios-instance-config');
var express = require('express');
var router = express.Router();
var controller = require('../controllers/agro-controller');

 /**
 * Retorna lista de itens de agro
 * @route GET /bff/agro/{item}/idsIn
 * @group agro - Endpoint Agro
 * @operationId agroItemIdsInGET
 * @produces application/json
 * @param {string} item.path.required - Tipo (doencas, insetos ou plantasdaninhas)
 * @param {string} ids.query.required - Lista de IDs dos tipos informados
 * @returns {object} 200 - Lista de itens de Agro (doencas, plantas, insetos)
 * @returns {Error}  500 - Erro ao tentar carregar lista de itens de agro
 * @security JWT
 */
router.get('/:item/idsIn', (req, res, next) => {
  // carrega lista de ids
  const ids = req.query['ids'].split(',');

  console.log(ids);

  // busca itens
  controller
    .getItensAgroByIds(req.params.item, ids)
    .then(itens => {
      res.status(200).send(itens);
    })
    .catch(err => {
      console.log(err);
      res.status(500).send([]);
    });
});

module.exports = router;
