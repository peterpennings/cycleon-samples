var axios = require('../config/axios-instance-config');

/**
 * Busca todas as fazendas do token autenticado
 */
exports.getFazendas = function() {
  return axios.get(`fazendas/`);
};

/**
 * Busca talhoes de uma fazenda
 */
exports.getTalhaoByFazenda = function(fazendaId) {
  return axios.get(`fazendas/${fazendaId}/talhoes`);
};

/**
 * Busca resumos de todos os talhoes de uma fazenda
 */
exports.getResumo = function(fazendaId) {
  return axios.get(`/fazendas/${fazendaId}/resumo`);
};
