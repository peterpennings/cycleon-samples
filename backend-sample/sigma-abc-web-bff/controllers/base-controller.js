var axios = require('../config/axios-instance-config');

/**
 * Busca uma pessoa por ID
 */
exports.getPessoaByCodigo = async function(pessoaId) {
  return await axios
    .get(`pessoas/${pessoaId}`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return null;
    });
};
