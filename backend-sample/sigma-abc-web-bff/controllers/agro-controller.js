var axios = require('../config/axios-instance-config');

const ITEM_DOENCAS = 'doencas';
const ITEM_INSETOS = 'insetos';

/**
 * Busca uma lista de doencas
 * @param {[number]} ids Lista de IDs de doencas
 * @returns {[object]} Lista de doencas
 */
exports.getDoencasByIds = async function(ids) {
  // promises para buscar em paralelo
  let promises = [];
  ids.forEach(id => {
    promises.push(getItemAgroById(ITEM_DOENCAS, id));
  });

  // executa promises em paralelo
  let doencas = [];
  await Promise.all(promises)
    .then(doencasResult => {
      doencasResult.forEach(doencaResponse => {
        doencas.push(doencaResponse.data);
      });
    })
    .catch(err => {
      console.log(err);
      return [];
    });
  return doencas;
};

/**
 * Busca lista de insetos
 */
exports.getInsetosByIds = async function(ids) {
  // promises para buscar em paralelo
  let promises = [];
  ids.forEach(id => {
    promises.push(getItemAgroById(ITEM_INSETOS, id));
  });

  // executa promises em paralelo
  let insetos = [];
  await Promise.all(promises)
    .then(insetosResult => {
      insetosResult.forEach(insetoResponse => {
        insetos.push(insetoResponse.data);
      });
    })
    .catch(err => {
      console.log(err);
      return [];
    });
  return insetos;
};

/**
 * Busca lista de itens de agro por ids
 */
exports.getItensAgroByIds = async function(item, ids) {
  // promises para buscar em paralelo
  let promises = [];
  ids.forEach(id => {
    promises.push(getItemAgroById(item, id));
  });

  // executa promises em paralelo
  let itens = [];
  await Promise.all(promises)
    .then(itensResult => {
      itensResult.forEach(itemResponse => {
        itens.push(itemResponse.data);
      });
    })
    .catch(err => {
      console.log(err);
      return [];
    });
  return itens;
};

/**
 * Busca item agro por ID
 * item: doenca, inseto, plantas etc.
 */
getItemAgroById = function(item, id) {
  return axios.get(`agro/${item}/${id}`);
};
