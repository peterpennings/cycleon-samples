var axios = require('../config/axios-instance-config');

exports.criarAtividade = async function(payload) {
  // post para servico
  return await axios
    .post(`fazendas/${payload.codFazenda}/ocorrencias/`, [payload])
    .then(function(response) {
      console.log(response);
    })
    .catch(function(error) {
      console.log(error);
    });
};

exports.getDoencasByOcorrencia = async function(ocorrencia) {
  // extrai ids das ocorrencias e cria Promises
  // para buscar em paralelo
  let promises = [];
  ocorrencia.doencas.forEach(doenca => {
    promises.push(getDoencaById(parseInt(doenca.doenca)));
  });

  // executa promises em paralelo
  let doencas = [];
  await Promise.all(promises)
    .then(doencasResult => {
      doencasResult.forEach(doencaResponse => {
        // extrai objeto doenca do response
        const doenca = doencaResponse.data;

        // cria objeto para UI adicionando nomes
        const doencaOcorrencia = ocorrencia.doencas.find(
          d => d.doenca == doenca.codigo
        );
        let doencaItem = {
          nome: doenca.nome,
          nome_cientifico: doenca.nomeCientifico,
          severidade: doencaOcorrencia.severidade,
          incidencia: doencaOcorrencia.incidencia
        };
        doencas.push(doencaItem);
      });
    })
    .catch(err => {
      console.log(err);
      return [];
    });
  return doencas;
};

getDoencaById = function(id) {
  return axios.get(`agro/doencas/${id}`);
};

exports.getOcorrencia = async function(fazendaId, ocorrenciaId) {
  return await axios.get(`fazendas/${fazendaId}/ocorrencias/${ocorrenciaId}`);
};

