/**
 * Configuracao para CORS
 *
 * Libera origin local e firebase ou sem origem (para caso de ferramentas)
 * TODO: Sempre verificar origin e nao permitir requests sem origin
 */

/**
 * Exporta configuracao do cors que deve
 * ser utilizada em todos os endpoints
 * dos routers
 */
module.exports = {
  origin: '*',
  credentials: true,
  origin: true
};
