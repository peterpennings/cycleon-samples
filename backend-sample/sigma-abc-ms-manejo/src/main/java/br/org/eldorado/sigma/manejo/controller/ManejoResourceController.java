package br.org.eldorado.sigma.manejo.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.org.eldorado.sigma.manejo.model.Manejo;
import br.org.eldorado.sigma.manejo.service.IManejoService;

/**
 * 
 * @author peter.pennings
 *
 */
@RestController
@RequestMapping(value = "/manejos")
@CrossOrigin
public class ManejoResourceController {

	@Autowired
	private IManejoService manejoService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<Manejo>> getManejos(
			@RequestParam(name = "talhao", required = true) Integer codigoTalhaoSafra,
			@RequestParam(name = "status", required = false) String status) {
		Boolean st = null;
		if ("true".equals(status)){
			st = true;
		} else if ("false".equals(status)) {
			st = false;
		}
		Collection<Manejo> resultList = manejoService.findByTalhaoSafraAndStatus(codigoTalhaoSafra, st);

		return new ResponseEntity<Collection<Manejo>>(resultList, HttpStatus.OK);
	}

	@RequestMapping(method = {RequestMethod.POST})
	public ResponseEntity<Collection<Manejo>> saveManejo(@RequestBody Collection<Manejo> manejos) {
		Collection<Manejo> resultList = manejoService.save(manejos);
		return new ResponseEntity<Collection<Manejo>>(resultList, HttpStatus.OK);
	}
	
	@RequestMapping(method = {RequestMethod.PUT})
	public ResponseEntity<Collection<Manejo>> updateManejo(@RequestBody Collection<Manejo> manejos) {
		Manejo manejo = null;
		for (Manejo mj : manejos) {
			manejo = manejoService.findById(mj.getCodigo());
			if(!manejo.getStatus()) {
				mj.setStatus(false);
			}
		}
		Collection<Manejo> resultList = manejoService.save(manejos);
		return new ResponseEntity<Collection<Manejo>>(resultList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ResponseEntity<Collection<br.org.eldorado.sigma.manejo.dto.Manejo>> getManejosByUserToken(@RequestAttribute("user_id") String userId){
		Integer uid = Integer.valueOf(userId);
		Collection<br.org.eldorado.sigma.manejo.dto.Manejo> resultList = manejoService.findByPessoa(uid);
		return new ResponseEntity<Collection<br.org.eldorado.sigma.manejo.dto.Manejo>>(resultList, HttpStatus.OK);
	}

}
