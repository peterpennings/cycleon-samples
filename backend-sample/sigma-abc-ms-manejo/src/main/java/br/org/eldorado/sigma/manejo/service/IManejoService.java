package br.org.eldorado.sigma.manejo.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import br.org.eldorado.sigma.manejo.model.Manejo;

@Service
public interface IManejoService {
	
	Collection<Manejo> save(Collection<Manejo> manejos);

	Collection<Manejo> findAll();

	Collection<Manejo> findByTalhaoSafraAndStatus(Integer codigoTalhaoSafra, Boolean status);
	
	Manejo findById(Integer id);
	
	Collection<br.org.eldorado.sigma.manejo.dto.Manejo> findByPessoa(Integer codigoPessoa);

}
