package br.org.eldorado.sigma.manejo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "tipo_manejo", schema = "sigma")
public class TipoManejo {

	@Id
	@Column(name = "cod_tipo_manejo")
	private Integer codigo;

	@Column(name = "nome")
	private String nome;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
