package br.org.eldorado.sigma.manejo.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.org.eldorado.sigma.manejo.model.Manejo;
import br.org.eldorado.sigma.manejo.repository.IManejoRepository;
import br.org.eldorado.sigma.manejo.service.IManejoService;

@Service
public class ManejoService implements IManejoService {

	@Autowired
	private IManejoRepository manejoRepository;
	
	public Manejo findById(Integer id) {
		return manejoRepository.findById(id).orElse(null);
	}

	@Override
	public Collection<Manejo> findAll() {
		return (Collection<Manejo>) manejoRepository.findAll();
	}

	@Override
	public Collection<Manejo> findByTalhaoSafraAndStatus(Integer codigoTalhaoSafra, Boolean status) {
		return manejoRepository.findByTalhaoSafraAndStatus(codigoTalhaoSafra, status);
	}
	

	@Override
	public Collection<br.org.eldorado.sigma.manejo.dto.Manejo> findByPessoa(Integer codigoPessoa) {
		return manejoRepository.blah(codigoPessoa);
	}

	@Override
	public Collection<Manejo> save(Collection<Manejo> manejos) {
		Collection<Manejo> result = new ArrayList<Manejo>();
		for (Manejo manejo : manejos) {
			if (manejo.getUuid() == null || manejo.getUuid() == "") {
				manejo.setUuid(UUID.randomUUID().toString());
			}

			if (manejo.getAdubo() != null
					&& (manejo.getAdubo().getUuid() == null || manejo.getAdubo().getUuid() == "")) {
				manejo.getAdubo().setUuid(UUID.randomUUID().toString());
			}
			
			if (manejo.getPlantio() != null
					&& (manejo.getPlantio().getUuid() == null || manejo.getPlantio().getUuid() == "")) {
				manejo.getPlantio().setUuid(UUID.randomUUID().toString());
			}
			
			if (manejo.getColheita() != null
					&& (manejo.getColheita().getUuid() == null || manejo.getColheita().getUuid() == "")) {
				manejo.getColheita().setUuid(UUID.randomUUID().toString());
			}
			
			if (manejo.getProduto() != null
					&& (manejo.getProduto().getUuid() == null || manejo.getProduto().getUuid() == "")) {
				manejo.getProduto().setUuid(UUID.randomUUID().toString());
			}
		}

		for (Manejo manejo : manejos) {
			result.add(manejoRepository.save(manejo));
		}

		return result;
	}

}
