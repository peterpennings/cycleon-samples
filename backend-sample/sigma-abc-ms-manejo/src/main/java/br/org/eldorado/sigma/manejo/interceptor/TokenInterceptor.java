package br.org.eldorado.sigma.manejo.interceptor;

import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 
 * @author peter.pennings
 *
 */
@Component
public class TokenInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// Variaveis de URL (Path)
		@SuppressWarnings("unchecked")
		Map<String, String> pathVariables = (Map<String, String>) request
				.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

		// Token (JWT)
		String auth = request.getHeader("Authorization");
		String[] tokenSplitted = auth.replace("Bearer", "").split("\\.");

		// Token Payload
		String payload = new String(Base64.getDecoder().decode(tokenSplitted[1]));
		JSONObject jsonObject = new JSONObject(payload);

		// Coloca o id do usuario na request
		String user_key = jsonObject.getString("user_id");
		request.setAttribute("user_id", user_key);

		return super.preHandle(request, response, handler);

	}

}
