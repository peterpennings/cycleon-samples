package br.org.eldorado.sigma.manejo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import br.org.eldorado.sigma.manejo.interceptor.TokenInterceptor;

/**
 * 
 * @author peter.pennings
 *
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
	private final List<String> urlsToIntercept = new ArrayList<String>() {

		private static final long serialVersionUID = 1L;

		{
			add("/manejos/user");
		}
	};

	@Bean
	TokenInterceptor tokenInterceptor() {
		return new TokenInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(tokenInterceptor()).addPathPatterns(urlsToIntercept);
		WebMvcConfigurer.super.addInterceptors(registry);
	}
}
