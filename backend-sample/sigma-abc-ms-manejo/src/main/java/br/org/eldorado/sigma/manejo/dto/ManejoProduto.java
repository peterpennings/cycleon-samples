package br.org.eldorado.sigma.manejo.dto;

import java.io.Serializable;

/**
 * 
 * @author peter.pennings
 *
 */
public class ManejoProduto implements Serializable {
	
	private Integer codigoManejo;

	private Integer codigoProduto;

	private String nomeProduto;

	private String nomeProdutoWs;

	private Integer codigoTipoProduto;

	private Double quantidadePorHa;

	private Double quantidadeTotal;

	private String uuid;

	public ManejoProduto(Integer codigoManejo, Integer codigoProduto, String nomeProduto, String nomeProdutoWs,
			Integer codigoTipoProduto, Double quantidadePorHa, Double quantidadeTotal, String uuid) {
		this.codigoManejo = codigoManejo;
		this.codigoProduto = codigoProduto;
		this.nomeProduto = nomeProduto;
		this.nomeProdutoWs = nomeProdutoWs;
		this.codigoTipoProduto = codigoTipoProduto;
		this.quantidadePorHa = quantidadePorHa;
		this.quantidadeTotal = quantidadeTotal;
		this.uuid = uuid;
	}

	public Integer getCodigoManejo() {
		return codigoManejo;
	}

	public void setCodigoManejo(Integer codigoManejo) {
		this.codigoManejo = codigoManejo;
	}



	public Integer getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(Integer codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public String getNomeProdutoWs() {
		return nomeProdutoWs;
	}

	public void setNomeProdutoWs(String nomeProdutoWs) {
		this.nomeProdutoWs = nomeProdutoWs;
	}

	public Integer getCodigoTipoProduto() {
		return codigoTipoProduto;
	}

	public void setCodigoTipoProduto(Integer codigoTipoProduto) {
		this.codigoTipoProduto = codigoTipoProduto;
	}

	public Double getQuantidadePorHa() {
		return quantidadePorHa;
	}

	public void setQuantidadePorHa(Double quantidadePorHa) {
		this.quantidadePorHa = quantidadePorHa;
	}

	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
