package br.org.eldorado.sigma.manejo.dto;

import java.io.Serializable;

/**
 * 
 * @author peter.pennings
 *
 */
public class ManejoAdubo implements Serializable {

	private Integer codigoManejo;
	
	private Integer codigoAdubo;

	private String nomeAdubo;

	private String nomeAduboWs;

	private Double quantidadePorHa;

	private Double quantidadeTotal;

	private String uuid;


	public ManejoAdubo(Integer codigoManejo, Integer codigoAdubo, String nomeAdubo, String nomeAduboWs,
			Double quantidadePorHa, Double quantidadeTotal, String uuid) {
		this.codigoManejo = codigoManejo;
		this.codigoAdubo = codigoAdubo;
		this.nomeAdubo = nomeAdubo;
		this.nomeAduboWs = nomeAduboWs;
		this.quantidadePorHa = quantidadePorHa;
		this.quantidadeTotal = quantidadeTotal;
		this.uuid = uuid;
	}



	public Integer getCodigoManejo() {
		return codigoManejo;
	}



	public void setCodigoManejo(Integer codigoManejo) {
		this.codigoManejo = codigoManejo;
	}



	public Integer getCodigoAdubo() {
		return codigoAdubo;
	}

	public void setCodigoAdubo(Integer codigoAdubo) {
		this.codigoAdubo = codigoAdubo;
	}

	public String getNomeAdubo() {
		return nomeAdubo;
	}

	public void setNomeAdubo(String nomeAdubo) {
		this.nomeAdubo = nomeAdubo;
	}

	public String getNomeAduboWs() {
		return nomeAduboWs;
	}

	public void setNomeAduboWs(String nomeAduboWs) {
		this.nomeAduboWs = nomeAduboWs;
	}

	public Double getQuantidadePorHa() {
		return quantidadePorHa;
	}

	public void setQuantidadePorHa(Double quantidadePorHa) {
		this.quantidadePorHa = quantidadePorHa;
	}

	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
