package br.org.eldorado.sigma.manejo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableTransactionManagement
public class ManejoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManejoApplication.class, args);
	}

}
