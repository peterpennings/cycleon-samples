package br.org.eldorado.sigma.manejo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "manejo_produto", schema = "sigma")
public class ManejoProduto implements Serializable {

	@Id
	@OneToOne
	@JoinColumn(name = "cod_manejo")
	@JsonBackReference
	private Manejo manejo;

	@Column(name = "cod_produto")
	private Integer codigoProduto;

	@Column(name = "nome_produto")
	private String nomeProduto;

	@Column(name = "nome_produto_ws")
	private String nomeProdutoWs;

	@Column(name = "cod_tipo_produto")
	private Integer codigoTipoProduto;

	@Column(name = "qtd_ha")
	private Double quantidadePorHa;

	@Column(name = "qtd_total")
	private Double quantidadeTotal;

	@Column(name = "uuid")
	private String uuid;

	public Manejo getManejo() {
		return manejo;
	}

	public void setManejo(Manejo manejo) {
		this.manejo = manejo;
	}

	public Integer getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(Integer codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public String getNomeProdutoWs() {
		return nomeProdutoWs;
	}

	public void setNomeProdutoWs(String nomeProdutoWs) {
		this.nomeProdutoWs = nomeProdutoWs;
	}

	public Integer getCodigoTipoProduto() {
		return codigoTipoProduto;
	}

	public void setCodigoTipoProduto(Integer codigoTipoProduto) {
		this.codigoTipoProduto = codigoTipoProduto;
	}

	public Double getQuantidadePorHa() {
		return quantidadePorHa;
	}

	public void setQuantidadePorHa(Double quantidadePorHa) {
		this.quantidadePorHa = quantidadePorHa;
	}

	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
