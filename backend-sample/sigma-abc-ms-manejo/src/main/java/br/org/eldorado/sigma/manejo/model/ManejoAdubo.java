package br.org.eldorado.sigma.manejo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "manejo_adubo", schema = "sigma")
public class ManejoAdubo implements Serializable {

	@Id
	@OneToOne
	@JoinColumn(name = "cod_manejo")
	@JsonBackReference
	private Manejo manejo;

	@Column(name = "cod_adubo")
	private Integer codigoAdubo;

	@Column(name = "nome_adubo")
	private String nomeAdubo;

	@Column(name = "nome_adubo_ws")
	private String nomeAduboWs;

	@Column(name = "qtd_ha")
	private Double quantidadePorHa;

	@Column(name = "qtd_total")
	private Double quantidadeTotal;

	@Column(name = "uuid")
	private String uuid;

	public Manejo getManejo() {
		return manejo;
	}

	public void setManejo(Manejo manejo) {
		this.manejo = manejo;
	}

	public Integer getCodigoAdubo() {
		return codigoAdubo;
	}

	public void setCodigoAdubo(Integer codigoAdubo) {
		this.codigoAdubo = codigoAdubo;
	}

	public String getNomeAdubo() {
		return nomeAdubo;
	}

	public void setNomeAdubo(String nomeAdubo) {
		this.nomeAdubo = nomeAdubo;
	}

	public String getNomeAduboWs() {
		return nomeAduboWs;
	}

	public void setNomeAduboWs(String nomeAduboWs) {
		this.nomeAduboWs = nomeAduboWs;
	}

	public Double getQuantidadePorHa() {
		return quantidadePorHa;
	}

	public void setQuantidadePorHa(Double quantidadePorHa) {
		this.quantidadePorHa = quantidadePorHa;
	}

	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
