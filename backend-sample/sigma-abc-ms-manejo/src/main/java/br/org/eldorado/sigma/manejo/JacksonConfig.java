package br.org.eldorado.sigma.manejo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bedatadriven.jackson.datatype.jts.JtsModule;

/**
 * 
 * @author peter.pennings
 *
 */
@Configuration
public class JacksonConfig {
	@Bean
	public JtsModule jtsModule() {
		return new JtsModule();
	}
}