package br.org.eldorado.sigma.manejo.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.org.eldorado.sigma.manejo.model.Manejo;

@Repository
public interface IManejoRepository extends CrudRepository<Manejo, Integer>, CustomManejoRepository {

	@Query("SELECT t FROM Manejo t "
			+ "WHERE t.codigoTalhaoSafra = :talhaoSafra "
			+ "AND ((t.status = :status) OR (:status IS NULL)) "
			+ "ORDER BY t.dataHora")
	Collection<Manejo> findByTalhaoSafraAndStatus(@Param("talhaoSafra") Integer codigoTalhaoSafra, @Param("status") Boolean status);
	
	@Query(value = "select m.* from sigma.manejo m "
			+ "inner join sigma.talhao_safra ts on ts.cod_talhao_safra = m.cod_talhao_safra "
			+ "inner join sigma.pessoa_fazenda pf on pf.cod_fazenda = ts.cod_fazenda "
			+ "where pf.cod_pessoa = 1071 and ts.cod_safra = 20192019", nativeQuery = true)
	Collection<Manejo> findByPessoa();
	

}
