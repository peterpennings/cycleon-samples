package br.org.eldorado.sigma.manejo.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.org.eldorado.sigma.manejo.dto.Manejo;
import br.org.eldorado.sigma.manejo.dto.ManejoAdubo;
import br.org.eldorado.sigma.manejo.dto.ManejoColheita;
import br.org.eldorado.sigma.manejo.dto.ManejoPlantio;
import br.org.eldorado.sigma.manejo.dto.ManejoProduto;
import br.org.eldorado.sigma.manejo.dto.TipoManejo;
import br.org.eldorado.sigma.manejo.repository.CustomManejoRepository;

@Repository
public class CustomManejoRepositoryImpl implements CustomManejoRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Collection<Manejo> blah(Integer codigoPessoa) {
		
		Query q = em.createNativeQuery("select " 
	    		+ "m.cod_manejo, m.cod_tipo_manejo, m.cod_talhao_safra, m.data_hora, m.data_hora_fim, m.the_geom, m.area_ha, m.status, m.id_semente_coop, m.uuid, m.observacoes, " 
	    		+ "ma.cod_manejo as adubo_cod_manejo, ma.cod_adubo, ma.nome_adubo, ma.nome_adubo_ws, ma.qtd_ha as adubo_qtd_ha, ma.qtd_total as adubo_qtd_total, ma.uuid as adubo_uuid, " 
	    		+ "mp.cod_manejo as produto_cod_manejo, mp.cod_produto, mp.nome_produto, mp.nome_produto_ws, mp.cod_tipo_produto, mp.qtd_ha as produto_qtd_ha, mp.qtd_total as produto_qtd_total, mp.uuid as produto_uuid, "
	    		+ "mpl.cod_manejo as plantio_cod_manejo, mpl.cod_cultura, mpl.cod_cultivar, mpl.nome_cultura_ws, mpl.nome_cultivar_ws, mpl.cod_tipo_plantio, mpl.cod_ciclo, mpl.cod_proposito_plantio, mpl.qtd_ha as plantio_qtd_ha, mpl.qtd_total as plantio_qtd_total, mpl.espacamento_entre_linhas_mt, mpl.ciclo_em_dias_previsto, mpl.uuid as plantio_uuid, "
	    		+ "mc.cod_manejo as colheita_cod_manejo, mc.qtd_ha as colheita_qtd_ha, mc.qtd_total as colheita_qtd_total, mc.stand_final, mc.uuid as colheita_uuid, "
	    		+ "tm.cod_tipo_manejo, tm.nome "
	    		+ "from sigma.manejo m "
	    		+ "inner join sigma.talhao_safra ts on ts.cod_talhao_safra = m.cod_talhao_safra "
	    		+ "inner join sigma.pessoa_fazenda pf on pf.cod_fazenda = ts.cod_fazenda "
	    		+ "inner join sigma.tipo_manejo tm on tm.cod_tipo_manejo = m.cod_tipo_manejo "
	    		+ "left join sigma.manejo_adubo ma on ma.cod_manejo = m.cod_manejo "
	    		+ "left join sigma.manejo_produto mp on mp.cod_manejo = m.cod_manejo "
	    		+ "left join sigma.manejo_plantio mpl on mpl.cod_manejo = m.cod_manejo "
	    		+ "left join sigma.manejo_colheita mc on mc.cod_manejo = m.cod_manejo "
	    		+ "where pf.cod_pessoa = :pessoa ", "manejoDtoMapping");
		
		q.setParameter("pessoa", codigoPessoa);
		
		List<Object[]> results = q.getResultList();
		
		Collection<Manejo> manejos = new ArrayList<Manejo>();
		results.stream().forEach((record) -> {
			Manejo manejo = (Manejo) record[0];
			ManejoAdubo manejoAdubo = (ManejoAdubo) record[1];
			ManejoProduto manejoProduto = (ManejoProduto) record[2];
			ManejoPlantio manejoPlantio = (ManejoPlantio) record[3];
			ManejoColheita manejoColheita = (ManejoColheita) record[4];
			TipoManejo tipoManejo = (TipoManejo) record[5];
			manejo.setAdubo(manejoAdubo.getCodigoManejo() == null ? null : manejoAdubo);
			manejo.setPlantio(manejoPlantio.getCodigoManejo() == null ? null : manejoPlantio);
			manejo.setColheita(manejoColheita.getCodigoManejo() == null ? null : manejoColheita);
			manejo.setProduto(manejoProduto.getCodigoManejo() == null ? null : manejoProduto);
			manejo.setTipoManejo(tipoManejo);
			manejos.add(manejo);
		});
		
		return manejos;
		/*Manejo manejos = em.find(Manejo.class, 306144);
		System.out.println(em);

		Query q = em.createNativeQuery(
				"SELECT b.id, b.title, b.price, a.firstName || ' ' || a.lastName as authorName FROM Book b JOIN Author a ON b.author_id = a.id WHERE b.title LIKE :title",
				"BookWithAuthorNamesMapping");
		q.setParameter("title", "%Hibernate Tips%");
		List<BookWithAuthorNames> books = q.getResultList();

		for (BookWithAuthorNames b : books) {
			log.info(b);
		}*/
	}

}
