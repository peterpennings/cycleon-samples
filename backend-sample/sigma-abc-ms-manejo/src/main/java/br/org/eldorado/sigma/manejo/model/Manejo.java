package br.org.eldorado.sigma.manejo.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vividsolutions.jts.geom.Geometry;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "manejo", schema = "sigma")
@SqlResultSetMapping(
        name = "manejoDtoMapping",
        		classes =  {
        				@ConstructorResult(
        						targetClass = br.org.eldorado.sigma.manejo.dto.Manejo.class, 
        						columns = {
        								@ColumnResult(name = "cod_manejo", type = Integer.class),
        								@ColumnResult(name = "cod_talhao_safra", type = Integer.class),
        								@ColumnResult(name = "data_hora", type = Date.class),
        								@ColumnResult(name = "data_hora_fim", type = Date.class),
        								@ColumnResult(name = "the_geom", type = Geometry.class),
        								@ColumnResult(name = "status", type = Boolean.class),
        								@ColumnResult(name = "area_ha", type = Double.class),
        								@ColumnResult(name = "observacoes", type = String.class),
        								@ColumnResult(name = "uuid", type = String.class),
        								@ColumnResult(name = "id_semente_coop", type = String.class)
        						}),
        				@ConstructorResult(
        						targetClass = br.org.eldorado.sigma.manejo.dto.ManejoAdubo.class, 
        						columns = {
        								@ColumnResult(name = "adubo_cod_manejo", type = Integer.class),
        								@ColumnResult(name = "cod_adubo", type = Integer.class),
        								@ColumnResult(name = "nome_adubo", type = String.class),
        								@ColumnResult(name = "nome_adubo_ws", type = String.class),
        								@ColumnResult(name = "adubo_qtd_ha", type = Double.class),
        								@ColumnResult(name = "adubo_qtd_total", type = Double.class),
        								@ColumnResult(name = "adubo_uuid", type = String.class)
        						}),
        				@ConstructorResult(
        						targetClass = br.org.eldorado.sigma.manejo.dto.ManejoProduto.class, 
        						columns = {
        								@ColumnResult(name = "produto_cod_manejo", type = Integer.class),
        								@ColumnResult(name = "cod_produto", type = Integer.class),
        								@ColumnResult(name = "nome_produto", type = String.class),
        								@ColumnResult(name = "nome_produto_ws", type = String.class),
        								@ColumnResult(name = "cod_tipo_produto", type = Integer.class),
        								@ColumnResult(name = "produto_qtd_ha", type = Double.class),
        								@ColumnResult(name = "produto_qtd_total", type = Double.class),
        								@ColumnResult(name = "produto_uuid", type = String.class)
        						}),
        				@ConstructorResult(
        						targetClass = br.org.eldorado.sigma.manejo.dto.ManejoPlantio.class, 
        						columns = {
        								@ColumnResult(name = "plantio_cod_manejo", type = Integer.class),
        								@ColumnResult(name = "cod_cultura", type = Integer.class),
        								@ColumnResult(name = "cod_cultivar", type = Integer.class),
        								@ColumnResult(name = "nome_cultura_ws", type = String.class),
        								@ColumnResult(name = "nome_cultivar_ws", type = String.class),
        								@ColumnResult(name = "cod_tipo_plantio", type = Integer.class),
        								@ColumnResult(name = "cod_ciclo", type = Integer.class),
        								@ColumnResult(name = "cod_proposito_plantio", type = Integer.class),
        								@ColumnResult(name = "plantio_qtd_ha", type = Double.class),
        								@ColumnResult(name = "plantio_qtd_total", type = Double.class),
        								@ColumnResult(name = "espacamento_entre_linhas_mt", type = Double.class),
        								@ColumnResult(name = "ciclo_em_dias_previsto", type = Integer.class),
        								@ColumnResult(name = "plantio_uuid", type = String.class)
        						}),
        				@ConstructorResult(
        						targetClass = br.org.eldorado.sigma.manejo.dto.ManejoColheita.class, 
        						columns = {
        								@ColumnResult(name = "colheita_cod_manejo", type = Integer.class),
        								@ColumnResult(name = "colheita_qtd_ha", type = Double.class),
        								@ColumnResult(name = "colheita_qtd_total", type = Double.class),
        								@ColumnResult(name = "stand_final", type = Double.class),
        								@ColumnResult(name = "colheita_uuid", type = String.class)
        						}),
        				@ConstructorResult(
        						targetClass = br.org.eldorado.sigma.manejo.dto.TipoManejo.class, 
        						columns = {
        								@ColumnResult(name = "cod_tipo_manejo", type = Integer.class),
        								@ColumnResult(name = "nome", type = String.class)
        						})
        				
        })
public class Manejo {

	@Id
	@Column(name = "cod_manejo")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "manejo_sequence")
	@SequenceGenerator(name = "manejo_sequence", sequenceName = "sigma.manejo_cod_manejo_seq", allocationSize = 1)
	private Integer codigo;

	@Column(name = "cod_talhao_safra")
	private Integer codigoTalhaoSafra;

	@Column(name = "data_hora")
	private Date dataHora;

	@Column(name = "data_hora_fim")
	private Date dataHoraFim;

	@Column(name = "the_geom")
	private Geometry local;

	@Column(name = "status")
	private Boolean status;

	@Column(name = "area_ha")
	private Double areaHa;

	@Column(name = "observacoes")
	private String observacoes;

	@ManyToOne
	@JoinColumn(name = "cod_tipo_manejo")
	private TipoManejo tipoManejo;

	@Column(name = "uuid")
	private String uuid;

	@OneToOne(mappedBy = "manejo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	private ManejoAdubo adubo;

	@OneToOne(mappedBy = "manejo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	private ManejoPlantio plantio;

	@OneToOne(mappedBy = "manejo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	private ManejoColheita colheita;

	@OneToOne(mappedBy = "manejo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonManagedReference
	private ManejoProduto produto;

	@Column(name = "id_semente_coop")
	private String codigoSementeCooperativa;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigoTalhaoSafra() {
		return codigoTalhaoSafra;
	}

	public void setCodigoTalhaoSafra(Integer codigoTalhaoSafra) {
		this.codigoTalhaoSafra = codigoTalhaoSafra;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public Date getDataHoraFim() {
		return dataHoraFim;
	}

	public void setDataHoraFim(Date dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}

	public Geometry getLocal() {
		return local;
	}

	public void setLocal(Geometry local) {
		this.local = local;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public TipoManejo getTipoManejo() {
		return tipoManejo;
	}

	public void setTipoManejo(TipoManejo tipoManejo) {
		this.tipoManejo = tipoManejo;
	}

	public ManejoAdubo getAdubo() {
		return adubo;
	}

	public void setAdubo(ManejoAdubo adubo) {
		this.adubo = adubo;
	}

	public ManejoPlantio getPlantio() {
		return plantio;
	}

	public void setPlantio(ManejoPlantio plantio) {
		this.plantio = plantio;
	}

	public ManejoColheita getColheita() {
		return colheita;
	}

	public void setColheita(ManejoColheita colheita) {
		this.colheita = colheita;
	}

	public ManejoProduto getProduto() {
		return produto;
	}

	public void setProduto(ManejoProduto produto) {
		this.produto = produto;
	}

	public String getCodigoSementeCooperativa() {
		return codigoSementeCooperativa;
	}

	public void setCodigoSementeCooperativa(String codigoSementeCooperativa) {
		this.codigoSementeCooperativa = codigoSementeCooperativa;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Double getAreaHa() {
		return areaHa;
	}

	public void setAreaHa(Double areaHa) {
		this.areaHa = areaHa;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

}
