package br.org.eldorado.sigma.manejo.dto;

import java.io.Serializable;

/**
 * 
 * @author peter.pennings
 *
 */
public class ManejoColheita implements Serializable {

	private Integer codigoManejo;

	private Double quantidadePorHa;

	private Double quantidadeTotal;

	private Double standFinal;

	private String uuid;
	
	public ManejoColheita(Integer codigoManejo, Double quantidadePorHa, Double quantidadeTotal, Double standFinal,
			String uuid) {
		this.codigoManejo = codigoManejo;
		this.quantidadePorHa = quantidadePorHa;
		this.quantidadeTotal = quantidadeTotal;
		this.standFinal = standFinal;
		this.uuid = uuid;
	}

	public Integer getCodigoManejo() {
		return codigoManejo;
	}

	public void setCodigoManejo(Integer codigoManejo) {
		this.codigoManejo = codigoManejo;
	}

	public Double getQuantidadePorHa() {
		return quantidadePorHa;
	}

	public void setQuantidadePorHa(Double quantidadePorHa) {
		this.quantidadePorHa = quantidadePorHa;
	}

	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public Double getStandFinal() {
		return standFinal;
	}

	public void setStandFinal(Double standFinal) {
		this.standFinal = standFinal;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
