package br.org.eldorado.sigma.manejo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "manejo_colheita", schema = "sigma")
public class ManejoColheita implements Serializable {

	@Id
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_manejo")
	@JsonBackReference
	private Manejo manejo;

	@Column(name = "qtd_ha")
	private Double quantidadePorHa;

	@Column(name = "qtd_total")
	private Double quantidadeTotal;

	@Column(name = "stand_final")
	private Double standFinal;

	@Column(name = "uuid")
	private String uuid;

	public Manejo getManejo() {
		return manejo;
	}

	public void setManejo(Manejo manejo) {
		this.manejo = manejo;
	}

	public Double getQuantidadePorHa() {
		return quantidadePorHa;
	}

	public void setQuantidadePorHa(Double quantidadePorHa) {
		this.quantidadePorHa = quantidadePorHa;
	}

	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public Double getStandFinal() {
		return standFinal;
	}

	public void setStandFinal(Double standFinal) {
		this.standFinal = standFinal;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
