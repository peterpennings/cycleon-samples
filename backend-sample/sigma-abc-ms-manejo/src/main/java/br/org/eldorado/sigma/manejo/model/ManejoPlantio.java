package br.org.eldorado.sigma.manejo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * @author peter.pennings
 *
 */
@Entity
@Table(name = "manejo_plantio", schema = "sigma")
public class ManejoPlantio implements Serializable {

	@Id
	@OneToOne
	@JoinColumn(name = "cod_manejo")
	@JsonBackReference
	private Manejo manejo;

	@Column(name = "cod_cultura")
	private Integer codigoCultura;

	@Column(name = "cod_cultivar")
	private Integer codigoCultivar;

	@Column(name = "nome_cultura_ws")
	private String nomeCulturaWs;

	@Column(name = "nome_cultivar_ws")
	private String nomeCultivarWs;

	@Column(name = "cod_tipo_plantio")
	private Integer codigoTipoPlantio;

	@Column(name = "cod_ciclo")
	private Integer codigoCiclo;

	@Column(name = "cod_proposito_plantio")
	private Integer codigoPropositoPlantio;

	@Column(name = "qtd_ha")
	private Double quantidadePorHa;

	@Column(name = "qtd_total")
	private Double quantidadeTotal;

	@Column(name = "espacamento_entre_linhas_mt")
	private Double espacamentoLinhas;

	@Column(name = "ciclo_em_dias_previsto")
	private Integer cicloDiasPrevisto;

	@Column(name = "uuid")
	private String uuid;

	public Manejo getManejo() {
		return manejo;
	}

	public void setManejo(Manejo manejo) {
		this.manejo = manejo;
	}

	public Integer getCodigoCultura() {
		return codigoCultura;
	}

	public void setCodigoCultura(Integer codigoCultura) {
		this.codigoCultura = codigoCultura;
	}

	public Integer getCodigoCultivar() {
		return codigoCultivar;
	}

	public void setCodigoCultivar(Integer codigoCultivar) {
		this.codigoCultivar = codigoCultivar;
	}

	public String getNomeCulturaWs() {
		return nomeCulturaWs;
	}

	public void setNomeCulturaWs(String nomeCulturaWs) {
		this.nomeCulturaWs = nomeCulturaWs;
	}

	public String getNomeCultivarWs() {
		return nomeCultivarWs;
	}

	public void setNomeCultivarWs(String nomeCultivarWs) {
		this.nomeCultivarWs = nomeCultivarWs;
	}

	public Integer getCodigoTipoPlantio() {
		return codigoTipoPlantio;
	}

	public void setCodigoTipoPlantio(Integer codigoTipoPlantio) {
		this.codigoTipoPlantio = codigoTipoPlantio;
	}

	public Integer getCodigoCiclo() {
		return codigoCiclo;
	}

	public void setCodigoCiclo(Integer codigoCiclo) {
		this.codigoCiclo = codigoCiclo;
	}

	public Integer getCodigoPropositoPlantio() {
		return codigoPropositoPlantio;
	}

	public void setCodigoPropositoPlantio(Integer codigoPropositoPlantio) {
		this.codigoPropositoPlantio = codigoPropositoPlantio;
	}

	public Double getQuantidadePorHa() {
		return quantidadePorHa;
	}

	public void setQuantidadePorHa(Double quantidadePorHa) {
		this.quantidadePorHa = quantidadePorHa;
	}

	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public Double getEspacamentoLinhas() {
		return espacamentoLinhas;
	}

	public void setEspacamentoLinhas(Double espacamentoLinhas) {
		this.espacamentoLinhas = espacamentoLinhas;
	}

	public Integer getCicloDiasPrevisto() {
		return cicloDiasPrevisto;
	}

	public void setCicloDiasPrevisto(Integer cicloDiasPrevisto) {
		this.cicloDiasPrevisto = cicloDiasPrevisto;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
