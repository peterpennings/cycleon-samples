package br.org.eldorado.sigma.manejo.dto;

import java.util.Date;

import com.vividsolutions.jts.geom.Geometry;

/**
 * 
 * @author peter.pennings
 *
 */
public class Manejo {

	private Integer codigo;

	private Integer codigoTalhaoSafra;

	private Date dataHora;

	private Date dataHoraFim;

	private Geometry local;

	private Boolean status;

	private Double areaHa;

	private String observacoes;

	private TipoManejo tipoManejo;

	private String uuid;

	private ManejoAdubo adubo;

	private ManejoPlantio plantio;

	private ManejoColheita colheita;

	private ManejoProduto produto;

	private String codigoSementeCooperativa;

	public Manejo(Integer codigo, Integer codigoTalhaoSafra, Date dataHora, Date dataHoraFim, Geometry local,
			Boolean status, Double areaHa, String observacoes, String uuid, String codigoSementeCooperativa) {
		this.codigo = codigo;
		this.codigoTalhaoSafra = codigoTalhaoSafra;
		this.dataHora = dataHora;
		this.dataHoraFim = dataHoraFim;
		this.local = local;
		this.status = status;
		this.areaHa = areaHa;
		this.observacoes = observacoes;
		this.uuid = uuid;
		this.codigoSementeCooperativa = codigoSementeCooperativa;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigoTalhaoSafra() {
		return codigoTalhaoSafra;
	}

	public void setCodigoTalhaoSafra(Integer codigoTalhaoSafra) {
		this.codigoTalhaoSafra = codigoTalhaoSafra;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public Date getDataHoraFim() {
		return dataHoraFim;
	}

	public void setDataHoraFim(Date dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}

	public Geometry getLocal() {
		return local;
	}

	public void setLocal(Geometry local) {
		this.local = local;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public TipoManejo getTipoManejo() {
		return tipoManejo;
	}

	public void setTipoManejo(TipoManejo tipoManejo) {
		this.tipoManejo = tipoManejo;
	}

	public ManejoAdubo getAdubo() {
		return adubo;
	}

	public void setAdubo(ManejoAdubo adubo) {
		this.adubo = adubo;
	}

	public ManejoPlantio getPlantio() {
		return plantio;
	}

	public void setPlantio(ManejoPlantio plantio) {
		this.plantio = plantio;
	}

	public ManejoColheita getColheita() {
		return colheita;
	}

	public void setColheita(ManejoColheita colheita) {
		this.colheita = colheita;
	}

	public ManejoProduto getProduto() {
		return produto;
	}

	public void setProduto(ManejoProduto produto) {
		this.produto = produto;
	}

	public String getCodigoSementeCooperativa() {
		return codigoSementeCooperativa;
	}

	public void setCodigoSementeCooperativa(String codigoSementeCooperativa) {
		this.codigoSementeCooperativa = codigoSementeCooperativa;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Double getAreaHa() {
		return areaHa;
	}

	public void setAreaHa(Double areaHa) {
		this.areaHa = areaHa;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

}
