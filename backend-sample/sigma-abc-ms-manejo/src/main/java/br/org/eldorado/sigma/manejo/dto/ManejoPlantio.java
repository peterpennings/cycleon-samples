package br.org.eldorado.sigma.manejo.dto;

import java.io.Serializable;

/**
 * 
 * @author peter.pennings
 *
 */
public class ManejoPlantio implements Serializable {
	
	private Integer codigoManejo;

	private Integer codigoCultura;

	private Integer codigoCultivar;

	private String nomeCulturaWs;

	private String nomeCultivarWs;

	private Integer codigoTipoPlantio;

	private Integer codigoCiclo;

	private Integer codigoPropositoPlantio;

	private Double quantidadePorHa;

	private Double quantidadeTotal;

	private Double espacamentoLinhas;

	private Integer cicloDiasPrevisto;

	private String uuid;

	public ManejoPlantio(Integer codigoManejo, Integer codigoCultura, Integer codigoCultivar, String nomeCulturaWs,
			String nomeCultivarWs, Integer codigoTipoPlantio, Integer codigoCiclo, Integer codigoPropositoPlantio,
			Double quantidadePorHa, Double quantidadeTotal, Double espacamentoLinhas, Integer cicloDiasPrevisto,
			String uuid) {
		this.codigoManejo = codigoManejo;
		this.codigoCultura = codigoCultura;
		this.codigoCultivar = codigoCultivar;
		this.nomeCulturaWs = nomeCulturaWs;
		this.nomeCultivarWs = nomeCultivarWs;
		this.codigoTipoPlantio = codigoTipoPlantio;
		this.codigoCiclo = codigoCiclo;
		this.codigoPropositoPlantio = codigoPropositoPlantio;
		this.quantidadePorHa = quantidadePorHa;
		this.quantidadeTotal = quantidadeTotal;
		this.espacamentoLinhas = espacamentoLinhas;
		this.cicloDiasPrevisto = cicloDiasPrevisto;
		this.uuid = uuid;
	}



	public Integer getCodigoManejo() {
		return codigoManejo;
	}



	public void setCodigoManejo(Integer codigoManejo) {
		this.codigoManejo = codigoManejo;
	}



	public Integer getCodigoCultura() {
		return codigoCultura;
	}

	public void setCodigoCultura(Integer codigoCultura) {
		this.codigoCultura = codigoCultura;
	}

	public Integer getCodigoCultivar() {
		return codigoCultivar;
	}

	public void setCodigoCultivar(Integer codigoCultivar) {
		this.codigoCultivar = codigoCultivar;
	}

	public String getNomeCulturaWs() {
		return nomeCulturaWs;
	}

	public void setNomeCulturaWs(String nomeCulturaWs) {
		this.nomeCulturaWs = nomeCulturaWs;
	}

	public String getNomeCultivarWs() {
		return nomeCultivarWs;
	}

	public void setNomeCultivarWs(String nomeCultivarWs) {
		this.nomeCultivarWs = nomeCultivarWs;
	}

	public Integer getCodigoTipoPlantio() {
		return codigoTipoPlantio;
	}

	public void setCodigoTipoPlantio(Integer codigoTipoPlantio) {
		this.codigoTipoPlantio = codigoTipoPlantio;
	}

	public Integer getCodigoCiclo() {
		return codigoCiclo;
	}

	public void setCodigoCiclo(Integer codigoCiclo) {
		this.codigoCiclo = codigoCiclo;
	}

	public Integer getCodigoPropositoPlantio() {
		return codigoPropositoPlantio;
	}

	public void setCodigoPropositoPlantio(Integer codigoPropositoPlantio) {
		this.codigoPropositoPlantio = codigoPropositoPlantio;
	}

	public Double getQuantidadePorHa() {
		return quantidadePorHa;
	}

	public void setQuantidadePorHa(Double quantidadePorHa) {
		this.quantidadePorHa = quantidadePorHa;
	}

	public Double getQuantidadeTotal() {
		return quantidadeTotal;
	}

	public void setQuantidadeTotal(Double quantidadeTotal) {
		this.quantidadeTotal = quantidadeTotal;
	}

	public Double getEspacamentoLinhas() {
		return espacamentoLinhas;
	}

	public void setEspacamentoLinhas(Double espacamentoLinhas) {
		this.espacamentoLinhas = espacamentoLinhas;
	}

	public Integer getCicloDiasPrevisto() {
		return cicloDiasPrevisto;
	}

	public void setCicloDiasPrevisto(Integer cicloDiasPrevisto) {
		this.cicloDiasPrevisto = cicloDiasPrevisto;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
