import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Variavel } from 'src/app/shared/models/clima/variavel';
import { Talhao } from 'src/app/shared/models/talhao';
import { ClimaService } from 'src/app/shared/services/clima.service';
import { Fazenda } from './../../shared/models/fazenda';
import { DashboardService } from './../../shared/services/dashboard.service';
import { BaseState } from './../../shared/store/base/base.reducers';

@Component({
  selector: 'sig-widget-previsao',
  templateUrl: './widget-previsao.component.html',
  styleUrls: ['./widget-previsao.component.scss']
})
export class WidgetPrevisaoComponent implements OnInit {
  renderChart = false;
  renderTalhoes = false;
  @Input() widget: any;

  config: any = {
    variavel: []
  };

  // variaveis possiveis para este widget
  variaveisDefault = [
    'precip',
    'tmax',
    'tmed',
    'tmin',
    'urmed',
    'rad',
    'vtmed2m',
    'vtmax2m',
    'dirvtmed10',
    'pressmed'
  ];

  loadedDatasets = [];

  // variavaies para escolher no widget
  variaveisDataset: Variavel[] = [];

  // fazenda e talhoes
  fazenda: Fazenda;
  talhoes: Talhao[];

  constructor(
    private store: Store<any>,
    private climaService: ClimaService,
    private dashboardService: DashboardService
  ) {}

  ngOnInit() {
    // busca lista de talhoes
    this.store
      .select('base')
      .subscribe((baseState: BaseState) => {
        /**
         * Um novo item e adicionado aos favoritos sempre
         * estara associado a fazenda selecionado. Portanto,
         * e seguro usar a fazenda da store.
         */
        this.fazenda = baseState.fazenda;
        this.talhoes = baseState.talhoes;
        this.renderTalhoes = true;
      })
      .unsubscribe();
    // unsub para evitar que mudancas de fazenda atualizem dados
    // para este componente

    // carrega grafico
    this._loadChart();
  }

  _loadChart() {
    // busca variaveis
    this.climaService
      .getVariaveis()
      .subscribe((variaveisReponse: Variavel[]) => {
        // filtra variaveis
        this.variaveisDataset = variaveisReponse.filter(
          v => this.variaveisDefault.indexOf(v.apelido) > -1
        );

        // carrega config e variavel inicial
        this.config = this.widget.options;

        // atualiza dataset de variaveis ativas
        if (this.config.variavel) {
          this.loadedDatasets = [
            ...this.loadedDatasets,
            this.config.variavel.apelido
          ];
        }

        this.renderChart = true;
      });
  }

  onTalhaoChange(talhao: Talhao) {
    this.renderChart = false;
    this.widget.talhao = talhao;
    this.renderChart = true;

    // atualiza no firebase
    this.dashboardService.updateWidget(this.widget.fid, this.widget);
  }

  onVariavelChange(variavel) {
    // verifica se esta adicionando ou removendo variavel
    if (variavel.source.checked) {
      this.loadedDatasets = [...this.loadedDatasets, variavel.source.id];
    } else {
      // remove variavel da memoria
      this.loadedDatasets = this.loadedDatasets.filter(
        it => it !== variavel.source.id
      );
    }
  }

  onConfigChange(newConfig) {
    this.config = newConfig;
    this.renderChart = false;

    // atualiza datasets
    this.loadedDatasets = [...this.loadedDatasets, newConfig.variavel.apelido];

    setTimeout(() => {
      this.renderChart = true;
    }, 1);
  }

  removeWidget(widget) {
    // remove da store e firebase
    this.dashboardService.removeWidget(widget);
  }
}
