import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetPrevisaoComponent } from './widget-previsao.component';

describe('WidgetPrevisaoComponent', () => {
  let component: WidgetPrevisaoComponent;
  let fixture: ComponentFixture<WidgetPrevisaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetPrevisaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetPrevisaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
